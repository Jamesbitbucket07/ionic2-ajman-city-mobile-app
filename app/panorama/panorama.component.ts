import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { TranslatePipe, TranslateService, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { Http, Headers } from '@angular/http';
declare var jQuery:any;

import { 
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar
} from '../shared'



@Component({
	templateUrl: 'build/panorama/panorama.component.html',
	 directives: [TopBar],
	 providers: [TranslateService, TranslateLoader, LanguageService, 
    { 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    }],
    pipes: [TranslatePipe]
})
export class Panorama implements OnInit {
	@ViewChild('panoramaViewer') panoramaViewer: ElementRef;
	imageSrc: any;
	images: any;
	isLoaded: boolean = false;
	selected: number;
	constructor(public viewCtrl:ViewController,public params: NavParams) {}

	ngOnInit() {
		this.images = this.params.get("images");
		this.imageSrc = this.images[0];
		this.selected = 0;
		console.log(this.panoramaViewer.nativeElement)
		this.isLoaded = true;
	}

	ionViewDidEnter() {
		console.log("Entered")
		jQuery("#panoramaViewer").cyclotron({continuous:0});
	}
	changePanorama(index) {
		// this.imageSrc = this.images[index];
		this.selected = index;
		jQuery("#panoramaViewer").trigger({ type: "reload:panorama", newBG: this.images[index]});
	}

	closeModal() {
		this.viewCtrl.dismiss();
	}
}