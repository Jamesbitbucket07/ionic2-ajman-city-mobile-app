import { Component, OnInit } from '@angular/core';
import { ViewController, NavParams, NavController, MenuController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { TranslatePipe, TranslateService, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { LanguageService } from "../shared/services/language.service";
import { VoiceButton } from './shared';

@Component({
	templateUrl: 'build/search/search.component.html',
	directives: [VoiceButton],
	providers: [TranslateService, TranslateLoader, LanguageService, 
    { 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    }],
    pipes: [TranslatePipe]
})


export class Search implements OnInit {
	listened: string;
	searchStr: string;
    constructor(private viewCtrl: ViewController, private menuController: MenuController,public languageService: TranslateService) {
		this.listened = "Hold down the button and speak your question"
	}

	ngOnInit() {
		this.languageService.use(window['localStorage']['currentLang']);
		this.menuController.open();
	}

	getListen(obj) {
		
		this.listened = (obj.str=="")?"Sorry! Try again.":obj.str;
		this.searchStr = obj.str;
		var that = this
		if(obj.str!=""&&obj.complete) {
			setTimeout(function() {
				that.menuController.open().then(() => {
					that.viewCtrl.dismiss({searchStr: that.searchStr});	
				});
			}, 400)
			
		}
		

	}
	close() {
        // console.log(this.hotelFilter);
        var that = this

        this.menuController.open().then(() => {
        	that.viewCtrl.dismiss();
        });
    }
}