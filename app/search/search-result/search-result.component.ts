import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NavController, ModalController, NavParams, MenuController } from 'ionic-angular';
import { 
	Hotels,
	Events,
	Attractions,
	Activities,
	Restaurants,
	HomePage,
	FavoriteItems,
	Settings,
	AuthorizationService,
	HttpClient,
	ContentService,
	ImageCache,
	DbService,
	Search,
	HotelDetails,
	TripDetails,
	AttractionDetails,
	EventDetails,
	ActivityDetails,
	RestaurantDetails,
	StaticContent,
	TravelAgents
} from '../../shared';

@Component({
	selector: "search-result",
	templateUrl: 'build/search/search-result/search-result.component.html',
	providers: [ImageCache, ContentService, DbService, HttpClient, AuthorizationService, NavController, ModalController],
})
export class SearchResult implements OnInit {
	@Input() results: any;
	@Output() closeResults = new EventEmitter<boolean>();
	@Output() openResults = new EventEmitter<any>();
	searchResult: any;
	loadedResults: boolean = false;
	constructor(public contentService: ContentService, public modalController: ModalController, private navController: NavController, private menuController: MenuController) {}

	ngOnInit() {
		this.searchResult = this.results
	}
	ngDoCheck() {
		if(this.searchResult!=this.results) {
			this.searchResult = this.results;
			this.loadedResults = true
		}
	}

	getDetails(item) {
      var detailReturn;
      if(item.alias=="hotel") {
        detailReturn  = HotelDetails;
      }
      if(item.alias=="itinerary") {
        detailReturn = TripDetails;
      }
      if(item.alias=="destination") {
        detailReturn = AttractionDetails;
      }
      if(item.alias=="event") {
        detailReturn = EventDetails;
      }
      if(item.alias=="activity") {
        detailReturn = ActivityDetails;
      }
      if(item.alias=="restaurant") {
        detailReturn = RestaurantDetails;
      }
      if(item.alias=="static") {
        detailReturn = StaticContent;
      }
      if(item.alias=="travelAgents") {
        detailReturn = TravelAgents;
      }
      return detailReturn;
    }
	
	close() {
		// alert("result close!");
		this.closeResults.emit(true);
	}
	openPage(elm) {
		var that = this;
		console.log(elm);
		this.openResults.emit({page: this.getDetails(elm), item: elm});
	}

}