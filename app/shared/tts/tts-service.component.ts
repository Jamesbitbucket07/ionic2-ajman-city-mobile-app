import { Component, OnInit, Input } from '@angular/core';

declare var TTS;


@Component({
	selector: "tts-button",
	templateUrl: 'build/shared/tts/tts-service.component.html'
})
export class TTSService {
	@Input() stringTTS: string;

	statusSpeak: boolean = false;
	constructor() {}

	ngOnDestroy() {
		this.statusSpeak = false;
		TTS.speak({
            text: ' ',
            locale: "en-GB",
            rate: 1.5
        },() => { console.log('Stopped') }, (reason: any) =>  { console.log(reason) });
	}
	startSpeak() {
		if(!this.statusSpeak) {
			this.statusSpeak = true;
			if(window['localStorage']['currentLang']=="ar") {
				var lang = "ar-ae";
			} else {
				var lang = "en-GB";
			}
			var strSpeak = this.getText(this.stringTTS);
			
			TTS.speak({
	            text: strSpeak,
	            locale: lang,
	            rate: 1.5
	        },() =>  { this.statusSpeak = false; }, (reason: any) => console.log(reason));
		} else {
			this.statusSpeak = false;
			TTS.speak({
	            text: ' ',
	            locale: "en-GB",
	            rate: 1.5
	        },() => { console.log('Stopped') }, (reason: any) =>  { console.log(reason) });
		}
	}
	getText(str) {
		return str.replace(/<[^>]+>/ig, "");
	}
}