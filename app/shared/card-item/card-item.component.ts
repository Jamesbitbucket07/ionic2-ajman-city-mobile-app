import { Component, Input, forwardRef } from '@angular/core';

import {
  FavoriteButton,
  HotelRating,
  Hotel
} from '../../shared';

@Component({
  selector: 'hotel-list-item',
  templateUrl: 'build/shared/card-list/card-list.component.html',
  directives:[forwardRef(() => FavoriteButton), forwardRef(() => HotelRating)]
})
export class HotelListItem {
    @Input() item: Hotel;
    showDetails = false;
    
    constructor(){
   
    }
  
    toggleCardDetails(){
        this.showDetails = !this.showDetails;
    }

}
