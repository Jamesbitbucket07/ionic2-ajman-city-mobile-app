// import { Component, forwardRef, Input, AfterViewChecked, Output, EventEmitter, ViewChild } from '@angular/core';
// import { chunk, concat } from 'lodash';
// import { Geolocation } from 'ionic-native';
// import { PopoverController, LoadingController, ModalController, Content } from 'ionic-angular';
// import { TranslatePipe } from 'ng2-translate/ng2-translate';

// import {
//     DbService,
//     AjmanMap,
//     FilterPopover,
//     CardListItem,
//     FavoriteItems,
//     Hotel,
//     Activity,
//     Restaurant,
//     Attraction
// } from '../'

// @Component({
//     selector: 'card-list',
//     templateUrl: 'build/shared/card-list/card-list.component.html',
//     providers: [
//         DbService
//     ],
//     pipes:[TranslatePipe],
//     directives: [forwardRef(() => AjmanMap), forwardRef(() => CardListItem)]

// })
// export class CardList  {
//     @ViewChild('cardScroll') cardScroll: Content;
//     @ViewChild('pageScroll') pageScroll: Content;
//     @Input() alias: string;
//     @Input() filters: boolean;
//     @Input() itemsFiltered: any = [];
//     @Input() sortBy: any;
//     @Input() showMap: boolean;
//     @Input() title: string;
//     @Input() filterFields: any;
//     @Input() filterValues: string;
//     @Output() dismissLoading = new EventEmitter<boolean>();
//     expanded: boolean;
//     items: Array<any>;
//     filteredItems: Array<any>;
//     limit: number;
//     lat: number;
//     lng: number;
//     currentPage: number;
//     pageLoaded: boolean = true;
//     pageSize:number;
//     disableCardScroll: boolean = true;
//     disablePageScroll: boolean = false;
//     constructor(public dbService: DbService, public popover: PopoverController, public loadingCtrl: LoadingController, private modalController : ModalController) {
//         this.expanded = false;
//         this.pageSize = 5;
//         this.currentPage = 0;
//         this.filteredItems = new Array<any>();
//     }
//     ngOnInit() {
//         console.log(this.filterValues)    
//         if(this.itemsFiltered != null && this.itemsFiltered.length > 0) {
//             this.items = chunk(this.sortItemsByDistance(this.itemsFiltered), this.limit);
//             console.log("itemsFiltered", this.itemsFiltered);
//             this.getItems(null)
//         } else {
//             this.getItems(null); 
//         }
        
//     }

//     ngAfterViewInit() {
     
//         this.cardScroll.addScrollListener((event) => {
//             console.log("cardScroll", event.target.scrollTop);
//             if(event.target.scrollTop>0) {
//                    this.disablePageScroll = true;
//                } else {
//                    this.disablePageScroll = false;
//                }
//         });
//         this.pageScroll.addScrollListener((event) => {
//                var topPosition = Math.ceil(event.target.clientHeight*.55)
//                console.log("pageScroll", {target: event, "scroll": event.target.scrollTop, "topPos": topPosition});
//                if(topPosition>event.target.scrollTop) {
//                    this.disableCardScroll = true;
//                } else {
//                    this.disableCardScroll = false;
//                }
//         });
//     }

//     hideLoading(e) {
//         this.dismissLoading.emit(true);
//     }
   
//     presentLoading() {
//       let loading = this.loadingCtrl.create({
//         content: "Please wait...",
//         duration: 3000,
//         dismissOnPageChange: false
//       });
//       loading.present();
//     }
   
//     private getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
//         var R = 6371; // Radius of the earth in km
//         var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
//         var dLon = this.deg2rad(lon2 - lon1);
//         var a =
//             Math.sin(dLat / 2) * Math.sin(dLat / 2) +
//             Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
//             Math.sin(dLon / 2) * Math.sin(dLon / 2);
//         var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//         var d = R * c; // Distance in km
//         return d;
//     }

//     private deg2rad(deg) {
//         return deg * (Math.PI / 180)
//     }

//     public getItems(filter) {

//         var items = new Array<any>();
//         var that = this;
//         // if (this.items) {
//         //     that.filteredItems = that.items[0];
//         //     console.log("Filterd", that.filteredItems)
//         // }
//         // else {
//             if(this.sortBy=="distance") {
//                 Geolocation.getCurrentPosition().then(pos => {
//                     console.log("here")
//                     this.lat = pos.coords.latitude;
//                     this.lng = pos.coords.longitude;
//                     var coords = { latitude: this.lat, longitude: this.lng };
//                     this.getFilteredItems();
//                 },err => {console.log("error",err)});
//             } else {
//                 console.log(this.alias) 
//                 this.getFilteredItems();
//             }
            
//         //}
//     }
//    private showFilter(event) {
//         var that = this;
//         let modal = this.modalController.create(FilterPopover, {type: this.alias, filter:this.filterValues});
//         modal.present();
//         modal.onDidDismiss(function(filter){
//             that.filterValues = filter;
//             that.currentPage = 0;
//             that.filteredItems.length=0;
//             that.getFilteredItems();
//         });
//     }

//    private getFilteredItems(){
//         var that = this;
        

//         var filter;
//         var fields;
//         switch(this.alias)
//         {
//             case "hotel":
//                 if(this.filterValues){
//                     filter = Hotel.getFilterStatement(this.filterValues);
//                     fields = this.filterFields;
//                 }
//                 else
//                 {
//                    filter =  {alias:"hotel"};
//                    fields = ["alias"];
//                 }
//                 that.dbService.getFiltered(filter, fields, this.currentPage * that.pageSize, that.pageSize)
//                 .subscribe(res => {
//                     that.filteredItems = that.filteredItems.concat(res);
//                 }); 
//                 break;
//             case "restaurant":
//                 if(this.filterValues){
//                     filter = Restaurant.getFilterStatement(this.filterValues);
//                     fields = this.filterFields;
//                 }
//                 else
//                 {
//                    filter =  {alias:"restaurant"};
//                    fields = ["alias"];
//                 }
                
//                 that.dbService.getFiltered(filter, fields, this.currentPage * that.pageSize, that.pageSize)
//                 .subscribe(res => {
//                     that.filteredItems = that.filteredItems.concat(res);
//                 }); 
//                 break;    
//             case "activity":
//                 if(this.filterValues){
//                      filter = Activity.getFilterStatement(this.filterValues);
//                      fields = this.filterFields;
//                 }
//                 else
//                 {
//                    filter =  {alias:"activity"};
//                    fields = ["alias"];
//                 }
               
//                 console.log(filter)
//                 that.dbService.getFiltered(filter, fields, this.currentPage * that.pageSize, that.pageSize)
//                 .subscribe(res => {
//                     that.filteredItems = that.filteredItems.concat(res);
//                 }); 
//                 break;
//              case "destination":
//                 if(this.filterValues){
//                      filter = Attraction.getFilterStatement(this.itemsFiltered, this.filterValues);
//                      fields = this.filterFields;
//                 }
//                 else
//                 {
//                    filter =  {alias:"destination"};
//                    fields = ["alias"];
//                 }
//                 console.log(filter)
//                 that.dbService.getFiltered(filter, fields, this.currentPage * that.pageSize, that.pageSize)
//                 .subscribe(res => {
//                     that.filteredItems = that.filteredItems.concat(res);
//                 }); 
//                 break;
//              case "event":
//                 filter = {alias:"event"};
//                 console.log(filter)
//                 that.dbService.getByIdsAndAlias(filter, ["alias"], this.currentPage * that.pageSize, that.pageSize)
//                 .subscribe(res => {
//                     that.filteredItems = that.filteredItems.concat(res);
//                 }); 
//                 break;
//             case "static":
//                 var  indexStr = "alias";
//                 if(this.itemsFiltered.length) {
//                     filter = {id: {$in: this.itemsFiltered } }; 
//                     indexStr = "id";
//                 } else {
//                     filter = {alias:"static"};
//                 }
                
//                 console.log(filter)
//                 that.dbService.getByIdsAndAlias(filter, [indexStr], this.currentPage * that.pageSize, that.pageSize + 20)
//                 .subscribe(res => {
//                     that.filteredItems = that.filteredItems.concat(res);
//                 }); 
//                 break;    
//             case "itinerary":
//                 filter = {alias:"itinerary"};
//                 console.log(filter)
//                 that.dbService.getFiltered(filter,["itenerary"], this.currentPage * that.pageSize, that.pageSize)
//                 .subscribe(res => {
//                     console.log(res)
//                     that.filteredItems = that.filteredItems.concat(res);
//                 }); 
//                 break;    
                
//             case "favorites":
//                 filter = FavoriteItems.getFilterStatement(this.itemsFiltered, this.filterValues);
//                 console.log(filter)
//                 that.dbService.getByIdsAndAlias(filter, this.filterFields, this.currentPage * that.pageSize, that.pageSize)
//                 .subscribe(res => {
//                     console.log(res)
//                     that.filteredItems = that.filteredItems.concat(res);
//                 }); 
//                 break;    
//         }
             
//     }

//     private flatten(arrayItems: Array<any>) {
//         var that = this;
//         return arrayItems.reduce(function(memo, el) {
//             var items = Array.isArray(el) ? that.flatten(el) : [el];
//             return memo.concat(items);
//         }, []);
//     }
//     private calculateDistance(results: Array<any>, coords: any) {
//         for (var index = 0; index < results.length; index++) {
//             var element = results[index];
//             var distance = Math.round(this.getDistanceFromLatLonInKm(parseFloat(coords.latitude), parseFloat(coords.longitude), element.latitude, element.longitude) * 1000);
//             element.distance = distance;
//         }
//         //this.items = chunk(this.sortItemsByDistance(results), this.limit);
//         this.filteredItems = this.sortItemsByDistance(results);
//     }
//     private sortItemsByDistance(res) {
//         return res.sort(function (a: any, b: any) {
//             if (a.distance < b.distance) return -1;
//             if (a.distance > b.distance) return 1;
//             return 0;
//         });
//     }

//     doInfinite(infiniteScroll) {
//         var that = this;
//         this.currentPage++;
//         this.getFilteredItems();

//     }
// }

import { Component, forwardRef, Input, AfterViewChecked, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { chunk, concat } from 'lodash';
import { Geolocation } from 'ionic-native';
import { PopoverController, LoadingController, ModalController, Content, Events} from 'ionic-angular';
import { TranslatePipe } from 'ng2-translate/ng2-translate';
import * as moment from 'moment';

import {
    DbService,
    AjmanMap,
    FilterPopover,
    CardListItem,
    FavoriteItems,
    Hotel,
    Activity,
    Restaurant,
    Attraction
} from '../'

@Component({
    selector: 'card-list',
    templateUrl: 'build/shared/card-list/card-list.component.html',
    providers: [
        DbService
    ],
    pipes:[TranslatePipe],
    directives: [forwardRef(() => AjmanMap), forwardRef(() => CardListItem)]

})
export class CardList  {
    @ViewChild('cardScroll') cardScroll: Content;
    @ViewChild('pageScroll') pageScroll: Content;
    // @ViewChild('pageScroll') pageScroll: ElementRef;
    @Input() alias: string;
    @Input() elmID: any = new Date().getTime();
    @Input() filters: boolean;
    @Input() itemsFiltered: any = [];
    @Input() sortBy: any;
    @Input() showMap: boolean;
    @Input() title: string;
    @Input() filterFields: any;
    @Input() filterValues: string;
    @Input() listHeader: boolean;
    @Input() iconHeader: string;
    @Input() descriptionHeader: string;
    @Input() titleHeader: string;
    @Input() isNearby: boolean = false;
    @Output() dismissLoading = new EventEmitter<boolean>();
    @Output() scrollBar = new EventEmitter<boolean>();
    expanded: boolean;
    items: Array<any>;
    filteredItems: Array<any>;
    limit: number;
    lat: number;
    lng: number;
    coords:any;
    currentPage: number;
    pageLoaded: boolean = true;
    pageSize:number;
    disableCardScroll: boolean = true;
    disablePageScroll: boolean = false;
    showInfinite: boolean = true;
    activeCard:string = "test";
    loadedPage: boolean = false;
    isScrolled: boolean = false;
    tripSelected: any = { "numOfDays": false, "interestTags": false, "activityTags": false };
    constructor(public events: Events, public dbService: DbService, public popover: PopoverController, public loadingCtrl: LoadingController, private modalController : ModalController) {
        this.expanded = false;
        this.pageSize = 10;
        this.currentPage = 0;
        this.filteredItems = new Array<any>();
        var that = this;
        this.events.subscribe("cardListItem:open", (cardItem) => {
            that.setActiveCard(cardItem);
        });
    }
    ngOnInit() {
        // console.log(this.filterValues)    
        // console.log(this.alias)    
        if(this.itemsFiltered != null && this.itemsFiltered.length > 0) {
            //this.items = chunk(this.sortItemsByDistance(this.itemsFiltered), this.limit);
            console.log("itemsFiltered", this.itemsFiltered);
            this.getItems(null)
        } else {
            this.getItems(null);
        }
        
    }

    ngOnDestroy() {
        var that = this;
        this.events.unsubscribe("cardListItem:open", (cardItem) => {
            that.setActiveCard(cardItem);
        });
    }
    setActiveCard(e) {
       var ac = e[0]
       this.activeCard = e[0]
    }

    ngAfterViewInit() {
         var that = this;
        // this.cardScroll.addScrollListener((event) => {
        //     console.log("cardScroll", event.target.scrollTop);
        //     if(event.target.scrollTop>0) {
        //            this.disablePageScroll = true;
        //        } else {
        //            this.disablePageScroll = false;
        //        }
        // });
        
        // this.pageScroll.nativeElement.addEventListener("scroll", function(event) {
        //     var topPosition = Math.ceil(event.target.clientHeight*.55)-3
        //        console.log("pageScroll", {target: event, "scroll": event.target.scrollTop, "topPos": topPosition});
        //        if(topPosition>event.target.scrollTop) {
        //            that.disableCardScroll = true;
        //        } else {
        //            that.disableCardScroll = false;
        //        }
        // });
        this.pageScroll.addScrollListener((event) => {
               var topPosition = Math.ceil(event.target.clientHeight*.55)
               // console.log("pageScroll", {target: event, "scroll": event.target.scrollTop, "topPos": topPosition});
               if(20<event.target.scrollTop) {
                   // this.disableCardScroll = true;
                   if(!that.isScrolled) {
                       that.isScrolled = true
                       that.scrollBar.emit(true);    
                   }
                   
               } else {
                   if(that.isScrolled) {
                       that.isScrolled = false
                       that.scrollBar.emit(false);    
                   }
               }
        });

    }

    hideLoading(e) {
        this.dismissLoading.emit(true);
    }
   
    presentLoading() {
      let loading = this.loadingCtrl.create({
        content: "Please wait...",
        duration: 3000,
        dismissOnPageChange: false
      });
      loading.present();
    }
   
    public getItems(filter) {
        // console.log(this.sortBy)
        var items = new Array<any>();
        var that = this;
        // if (this.items) {
        //     that.filteredItems = that.items[0];
        //     console.log("Filterd", that.filteredItems)
        // }
        // else {

            if(this.sortBy=="distance") {
                Geolocation.getCurrentPosition().then(pos => {
                    that.lat = pos.coords.latitude;
                    that.lng = pos.coords.longitude;
                    that.coords = { latitude: that.lat, longitude: that.lng };
                    this.getFilteredItems().subscribe(res => {
                        if(this.alias=="event"){
                            var today = new Date();
                            var itemsList = res.items.filter(obj => {
                                var evenDate = new Date(obj.eventStartDate)
                                return moment(evenDate).isAfter(today);                        
                            })
                        } else {
                            var itemsList = res.items
                        }

                        that.filteredItems = that.filteredItems.concat(itemsList);
                        if(that.filteredItems.length==res.totalItems) {
                            that.showInfinite = false;
                        }
                        
                        console.log("total", res.totalItems)
                        if(res.totalItems==0) {
                            that.showInfinite = false;
                        }
                        
                        that.loadedPage = true;
                        
                    });
                },err => {that.loadedPage = true; console.log("error",err)});
            } else {

               this.getFilteredItems().subscribe(res => {
                   
                    if(this.alias=="event"){
                        var today = new Date();
                        var itemsList = res.items.filter(obj => {
                            var evenDate = new Date(obj.eventStartDate)
                            return moment(evenDate).isAfter(today);
                        })
                    } else {
                        var itemsList = res.items
                    }

                    that.filteredItems = that.filteredItems.concat(itemsList);
                    if(that.filteredItems.length==res.totalItems) {
                        that.showInfinite = false;
                    }
                    console.log("total", res.totalItems)
                    if(res.totalItems==0) {
                        that.showInfinite = false;
                    }
                    that.loadedPage = true;
                    
                }, err => { that.loadedPage = true; });
            }
            
        //}
    }
   private showFilter(event) {
        var that = this;
        let modal = this.modalController.create(FilterPopover, {type: this.alias, filter:this.filterValues}, {showBackdrop: true, enableBackdropDismiss: true});
        modal.present();
        modal.onDidDismiss(function(filter){
            filter.forEach(f => {
                that.filterValues[f["field"]] = f.checked;
            });
            //that.filterValues = filter;
            that.currentPage = 0;
            that.filteredItems.length=0;
            that.getFilteredItems().subscribe(res => {

                that.filteredItems = that.filteredItems.concat(res.items);
                if(that.filteredItems.length==res.totalItems) {
                    that.showInfinite = false;
                }

            });
        });
    }

     private showItineraryFilter(event,type) {
        var that = this;
        let modal = this.modalController.create(FilterPopover, {type: type, filter:this.filterValues}, {showBackdrop: true, enableBackdropDismiss: true});
        modal.present();
        this.tripSelected = { "numOfDays": false, "interestTags": false, "activityTags": false };
        modal.onDidDismiss(function(filter){
           
            filter.forEach(f => {
               that.filterValues[f["field"]] = f.checked;
               
               if(f.checked && that.tripSelected[f["field"]]==false) {
                   that.tripSelected[f["field"]] = true;
               }
            });
            console.log("filterTrip", that.tripSelected)
            that.currentPage = 0;
            that.filteredItems.length=0;
            // console.log(filter)
            that.getFilteredItems().subscribe(res => {

                that.filteredItems = that.filteredItems.concat(res.items);


            });
        });
    }

   private getFilteredItems(){
        var that = this;
        var filter;
        var fields;
        var selector:any;
        var sortFunc:any;
        var filterType  = 'or';
        switch(this.alias)
        {
            case "hotel":
            case "travelAgent":
            case "restaurant":
            case "activity":
            case "destination":
            case "event":
                filter = this.filterValues;
                fields = this.filterFields;
                selector = {alias:this.alias};
                break;
            case "itinerary":
                filter = this.filterValues;
                fields = this.filterFields;
                selector = {alias:this.alias};
                filterType = 'and';
                break;    
            case "static":
                var  indexStr = "alias";
                if(this.itemsFiltered && this.itemsFiltered.length) {
                    filter = null;
                    fields = ['id'];
                    selector = {id: {$in: this.itemsFiltered } }; 
                    indexStr = "id";
                } else {
                    filter = {alias:"static"};
                }
                fields = [indexStr];
              
            break;   
            case "favorites":
                filter = this.filterValues;
                fields = this.filterFields;
                selector = {id: { $in: this.itemsFiltered }};
                break; 
            case "all":
                filter = this.filterValues;
                fields = this.filterFields;
                selector = {alias: {$in: ['hotel', 'destination', 'restaurant', 'activity', 'event'] } };
            break  
        }
        return that.dbService.getFiltered(selector, filter, fields, this.currentPage * that.pageSize, that.pageSize, that.sortBy,filterType, this.coords);

             
    }
    doInfinite(infiniteScroll) {
        var that = this;
        this.currentPage++;
        this.getFilteredItems().subscribe(res => {
            console.log("infinite", res);
            var items = res.items;

            if(this.alias=="event"){
                var today = new Date();
                var itemsList = res.items.filter(obj => {
                    var evenDate = new Date(obj.eventStartDate)
                    return moment(evenDate).isAfter(today);
                })
            } else {
                var itemsList = res.items
            }

            that.filteredItems = that.filteredItems.concat(itemsList);

            
            if(that.filteredItems.length==res.totalItems) {
                that.showInfinite = false;
            }
            infiniteScroll.complete();
        });

    }
}