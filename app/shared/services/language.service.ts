import {Injectable} from '@angular/core';
import {Storage, LocalStorage} from 'ionic-angular';
import { TranslateService, TranslatePipe } from 'ng2-translate/ng2-translate';

import {AppSettings} from '../';
import '../helpers/extensions';

@Injectable()
export class LanguageService {
  translate: any;
  translation: string;
  local: any;

  constructor(translate: TranslateService) {
  	this.translate = translate;
  	this.translation = "";
  	this.local = new Storage(LocalStorage);
  	if(window['localStorage']['langChangeSubscribed']!="true") {
  		this.local.set("langChangeSubscribed", "true");
		this.translate.onLangChange.subscribe((event) => {
	  		console.log(event);
	  
	  		this.local.set("currentLang", event.lang);
	  		this.local.set("translations", JSON.stringify(event.translations));
		});
	}
  }
  

  setLanguage(lang) {
  	var that = this;
  	this.translate.use(lang);
  	
  }

  getCurrentLang() {
  	return this.local.get("currentLang");
  }

  getTranslation(str) {
  	// var pr = this.translate.get(str).toPromise().then(res => { return res; });
  	var t = JSON.parse(window['localStorage']['translations']);
  	console.log(t[str])
  	return (t[str])?t[str]:str;
  }
 }