import { Observable } from 'rxjs/Observable';

export interface Authorizable
{
    authorize() : Observable<string>;
    tokenHasExpired() : boolean;
}