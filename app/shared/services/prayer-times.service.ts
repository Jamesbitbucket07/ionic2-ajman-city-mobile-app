import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import {
  AppSettings
} from '../';


/**
 * PrayerTimes
 */
@Injectable()
export class PrayerTimes {
    prayers: any;
    constructor(public http: Http) {

    }
    getPrayerTimes(){
        var apiUrl = AppSettings.PRAYER_API;
        apiUrl = apiUrl.replace(/{{time}}/g,new Date().getTime().toString());
        if (this.prayers) {
            return Observable.create(observer => {
                // console.log("from cache")
                observer.next(this.prayers);
            });
        }
        return Observable.create(observer => {
            this.http.get(apiUrl)
            .map((res) => res.json())
            .subscribe(res => {
                // console.log("from live 1", res)                
                this.prayers = res;
                observer.next(this.prayers);
            },
                err => console.log(err));
        });
       
    }

}
