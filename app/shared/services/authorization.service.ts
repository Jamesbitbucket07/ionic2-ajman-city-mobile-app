import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import {
  AppSettings,
} from '../';



@Injectable()
export class AuthorizationService{
  
  constructor(public http: Http) {
  }

  authorize() : Observable<string> {
    //if token is still valid return it, or fetch it from the server
    //in both cases return an observable that the requesting service can subscribe to 
    if(!this.tokenHasExpired()){
        //token is valid, return it from storage
       return Observable.create(observer => {
            observer.next(localStorage.getItem('access_token'));
            observer.complete();        
       });
    }
    
    //token is not valid. request a new one from the server
    var creds = `username=${AppSettings.API_USERNAME}&password=${AppSettings.API_PASSWORD}&grant_type=${AppSettings.API_GRANT_TYPE}`;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(AppSettings.API_ENDPOINT + AppSettings.API_TOKEN, creds, {
        headers: headers
        })
        .map((res : any) => {
            let data = res.json();
            localStorage.setItem('access_token', data.access_token);
            localStorage.setItem('token_expiry', Math.floor(new Date().getTime() / 1000) + data.expires_in);
            return data.access_token;
        });
  }

  //check if we have a token and that it is still valid
  tokenHasExpired() : boolean{
    if(!localStorage.getItem('token_expiry') || !localStorage.getItem('access_token'))
    {
      return true;
    }
    let expireTime = parseInt(localStorage.getItem('token_expiry'));
    let currentTime: number = (Math.floor(new Date().getTime() / 1000));
     if(expireTime < currentTime){
      return true;
    }
    return false;  
  }
  
}

