import { Injectable, Inject } from '@angular/core';
import { Transfer, File } from 'ionic-native';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/forkJoin';
import { ConnectivityService } from '../';

declare var cordova;

@Injectable()
export class FileService {
	storageDirectory: string = cordova.file.cacheDirectory;
	networkService:ConnectivityService;
	constructor(@Inject(ConnectivityService) network: ConnectivityService) {
		this.networkService = network;
	}

	checkThenDownloadFile(url, file, directory) {
		return Observable.create(observer =>{
			File.checkDir(this.storageDirectory, directory).then(res => {
				File.checkFile(this.storageDirectory+'/'+directory, file).then(res => {
					var path  = this.storageDirectory+directory+'/'+file;
					observer.next(path);
				}).catch(err => {
					console.log("checkTheDownloadFile:checkFile", err);
					
	                 this.downloadFileAsync(url,directory).subscribe(function (res) {
	                   observer.next(res);
	                 }, function (err) {
	                    console.error("checkTheDownloadFile:downloadFileAsync", err);
	                 })
				})
			}).catch(err => { 
				console.log("checkTheDownloadFile:checkDir",err);
				File.createDir(this.storageDirectory, directory, true).then(res => {
					
					this.downloadFileAsync(url,directory).subscribe(function (res) {
	                    observer.next(res);
	                 }, err => {
	                 	console.log("downloadFileAsync:checkDir",err);
	                 })
				})
			})
		})

	}

	getFile(url, directory) {
		return Observable.create(observer => {
			var fileName = url.split('//').pop().split('/').pop();
	        if (this.networkService.isOnline()) {
	            this.checkThenDownloadFile(url, fileName, directory)
	                .subscribe(function (res) {
	                   console.log("ImgOnline", res)
	                   observer.next(res);
	                }, function (err) {
	                    //error
	                })
	        }else{
	            this.getOfflineData(fileName, directory).subscribe( res => {
	                console.log("ImgOffline", res);
	                observer.next(res);
	            })
	        }
		})
		
	}


	downloadFileAsync(url,directory) {
		return Observable.create(observer => {
			const fileTransfer = new Transfer();
			var options = {create : true, exclusive: false};
			var fileName = url.split('//').pop().split('/').pop();
			var deviceUrl = this.storageDirectory + directory +'/'+ fileName;
			fileTransfer.download(url, deviceUrl, true).then(res => {
				observer.next(res.nativeURL);
			}).catch(err => {
				console.log("downloadFileAsync:download:error", err, deviceUrl);
				
			});
		})
		
	}
	getOfflineData(file, directory) {
		return Observable.create(observer => {
			File.checkDir(this.storageDirectory, directory).then(res => {
				File.checkFile(this.storageDirectory+directory, file).then(res => {
					 var path = this.storageDirectory+directory+'/'+file;
					 observer.next(path);
				}, err => { console.log("getOffilneData:checkFile", err, this.storageDirectory+directory+'/'+file); })
			}, err => { console.log("getOffilneData:checkDir", err); })
		});
	}
}