import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import {
  AppSettings
} from '../';

/**
 * Weather
 */
@Injectable()
export class Weather {
    currentWeather: any;
    constructor(public http: Http) {

    }
    getWeather(){
        if (this.currentWeather) {
            return Observable.create(observer => {
                // console.log("from cache")
                observer.next(this.currentWeather);
            });
        }
        return Observable.create(observer => {
            this.http.get(AppSettings.WEATHER_API)
            .subscribe(res => {
                // console.log("from live 2")                
                this.currentWeather = res;
                observer.next(this.currentWeather);
            },
                err => console.log(err));
            
        });
       
    }

}