import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import {
    AppSettings,
    DbService,
    ImageCache,
    Hotel,
    Trip,
    Attraction,
    Activity,
    Restaurant,
    Event, 
    TextPage
} from '../';

import '../helpers/extensions';

@Injectable()
export class ContentService {

    private _db;
    private _items: Array<any>;
    private _imageCache: any;
    constructor( @Inject(DbService) dbService: DbService, imageCache: ImageCache) {
        this._db = dbService;
        this._imageCache = imageCache;
    }
    search(query:string){
      var that = this;  
      return Observable.create(observer => {
          this._db.search(query).subscribe(res => {
                var allItems = res;
                observer.next(allItems);
            });
      });
    }

    findIds(id) {
        return this._db.findById(id);
    }

    getByIds(ids:Array<number>){
      var that = this;  
      return Observable.create(observer => {
        this._db.getByIds(ids).subscribe(res => {
            var allItems = res;
            observer.next(allItems);

        });
      });
    }
    getAll(alias: string) {
        var that = this;
        return Observable.create(observer => {
            var resultObserver = this._db.getAll(alias);
                resultObserver.subscribe(res => {
                    var allItems = res;
                    this._items = allItems;
                    observer.next(allItems);
                },
                err => console.log(err),
                () => console.log("completed"));
        });
    }

    getFilteredList(filter:any) {
        var that = this;
        return Observable.create(observer => {
            var resultObserver = this._db.getFiltered(filter);
                resultObserver.subscribe(res => {
                    var allItems = res;
                    this._items = allItems;
                    observer.next(allItems);
                },
                err => console.log(err),
                () => console.log("completed"));
        });
    }


    private getMappedClasses(res:Array<any>){
        var that = this;
        var allItems = new Array<any>();

        // res.forEach(element => {
        //     switch(element.alias)
        //     {
        //         case "hotel":
        //             allItems.push(new Hotel(element, that._imageCache));
        //             break;
        //         case "itinerary":
        //             allItems.push(new Trip(element, that._imageCache));
        //             break;
        //             case "destination":
        //             allItems.push(new Attraction(element, that._imageCache));
        //             break;    
        //             case "event":
        //             allItems.push(new Event(element, that._imageCache));
        //             break;   
        //             case "activity":
        //             allItems.push(new Activity(element, that._imageCache));
        //             break;      
        //         case "restaurant":
        //             allItems.push(new Restaurant(element, that._imageCache));
        //             break; 
        //         case "TextPage":
        //             allItems.push(new TextPage(element, that._imageCache));
        //             break; 
        //         case "travelAgents":
        //             if(element.id == 1256) {
        //                 allItems.push(new TextPage(element, that._imageCache));    
        //             }
        //             break;              
        //     }
            
        // });
        // return allItems;
     
    }
    destroyDB(db: string) {
        this._db.destroyDB(db);
    }
    getItemById(id: number) {
        return this._items.find(h => h.id.toString() === id)
    }
    getItemsByIds(ids: any) {
        var checkedIds = {};
        var that = this;
        ids.forEach(function(e, index) {
           var t = that._items.find(h => h.id.toString() === e);
           checkedIds[index] = t;
           // checkedIds.push(t);
        })
        return checkedIds;
    }
    getItemByName(name: string) {
        return this._items.find(h => h.name === name)
    }
    getFeaturedItems() {
        return this._items.filter(h => h.featured)
    }

}

