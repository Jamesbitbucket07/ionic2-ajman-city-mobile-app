import { Injectable } from '@angular/core';

@Injectable()
export class Favorites{

    constructor(){
        if(!this.getAllFavorites()) {
            this.resetFavorites();
        }
        
    }
    // favorites will have the (_id)s of the db element, example hotel_1334
    getAllFavorites(){
        return localStorage.getItem('favorites') ? JSON.parse(localStorage.getItem('favorites')): null;
    }

    saveFavorites(favs:Array<number>){
        localStorage.setItem('favorites', JSON.stringify(favs));
    }
    
    addToFavorites(item:number){

        var currentFavorites = this.getAllFavorites();

        if(currentFavorites.indexOf(item) < 0)
        {
            currentFavorites.push(item);
            this.saveFavorites(currentFavorites);
        }

    }

    removeFromFavorites(item:number){

        var currentFavorites = this.getAllFavorites();

        if(currentFavorites && currentFavorites.indexOf(item) > -1)
        {
            currentFavorites.splice(currentFavorites.indexOf(item),1);
            this.saveFavorites(currentFavorites);
        }

    }

    resetFavorites(){
        this.saveFavorites([]);
    }

    itemIsFavorite(item){
        var currentFavorites = this.getAllFavorites();
        return currentFavorites.indexOf(item) > -1;
    }

    //type can be hotel, restaurant, activity, destination, etc.
    getFavoritesByType(type:string){
        var currentFavorites = this.getAllFavorites();
        var results = Array<number>();
        currentFavorites.forEach(element => {
            //get the type portion of the id ex. hotel_1334, will return hotel
            var elementType = element.split('_')[0];
            if(type === elementType){
                results.push(element);
            }
        });
        return results;
    }

}