import { Component, OnInit, Input } from '@angular/core';
import { File } from 'ionic-native';
import { ConnectivityService, AppSettings, FileService } from '../';

@Component({
	selector: '[image-saver]',
	providers: [FileService],
	template: '<img [src]="fileSource">'
})
export class ImageSaver implements OnInit {
	@Input() sourceFile: string;
	@Input() directory: string;
	fileSource: string = null;

	constructor(public connectivityService: ConnectivityService, public fileServices: FileService) {}

	ngOnInit() {
		var that = this;
		var fileName = this.sourceFile.split('//').pop().split('/').pop();
		console.log("fileName", fileName);
        if (this.connectivityService.isOnline()) {
            this.fileServices.checkThenDownloadFile(this.sourceFile, fileName, this.directory)
                .subscribe(function (res) {
                   console.log("ImgOnline", res)
                   that.fileSource = res;
                }, function (err) {
                    console.log("ImgOnline:error", err)
                })
        }else{
            this.fileServices.getOfflineData(fileName, this.directory).subscribe( res => {
                console.log("ImgOffline", res);
                that.fileSource = res;
            })
        }
	}
}