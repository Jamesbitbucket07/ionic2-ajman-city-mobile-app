import '../helpers/extensions';
export class AppSettings {
   public static get API_ENDPOINT(): string { return 'http://40.76.212.209:8022/umbraco/'; }
   public static get SERVER_URL(): string { return 'http://ajman.travel'; }
   public static get API_USERNAME(): string { return 'readonly_user'; }
   public static get API_PASSWORD(): string { return 'uQrJodGm68VKko'; }
   public static get API_GRANT_TYPE(): string { return 'password'; }
   public static get API_TOKEN(): string { return 'oauth/token'; }
   public static get API_CONTENT_ROOT(): string { return 'rest/v1/content/'; }
   public static get API_MEDIA_ROOT(): string { return 'rest/v1/media/'; }
   public static get AJMAN_LOCATION_LATITUDE(): number { return 25.3995413; }
   public static get AJMAN_LOCATION_LONGITUDE(): number { return 55.4603631; }
   public static get WEATHER_API(): string {return 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22Ajman%2CAE%22%20)%20and%20u%3D%22c%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys';}
   public static get PRAYER_API(): string {return 'http://api.aladhan.com/timings/{{time}}?latitude=25.3995413&longitude=55.4603631&timezonestring=Asia/Dubai&method=5';}
   public static get API_IDS(): Array<any> { 
       return [
                {
                    DB_ENTITY: 'hotel',
                    ENTITY_API: '1152/descendants'
                },
                {
                    DB_ENTITY: 'attractions_en',
                    ENTITY_API: '1319/descendants'
                },
                {
                    DB_ENTITY: 'activities_en',
                    ENTITY_API: '1352/descendants'
                },
                {
                    DB_ENTITY: 'events_en',
                    ENTITY_API: '1399/descendants'
                },
                {
                    DB_ENTITY: 'restaurants_en',
                    ENTITY_API: '1267/descendants'
                },
                {
                    DB_ENTITY: 'itinerary_en',
                    ENTITY_API: '1447/descendants'
                },
                {
                    DB_ENTITY: 'facts_en',
                    ENTITY_API: '1343/descendants'
                },
                {
                    DB_ENTITY: 'travels_en',
                    ENTITY_API: '1143/descendants'
                }
       ]
    }
    public static API_BY_NAME(name :string): any { 

        return AppSettings.API_IDS.find(item => item.DB_ENTITY === name)
       
    }

    public static API_FULL_URL_BY_NAME(name :string): string { 

        var lang = localStorage.getItem("currentLang") == "en" ? "" : "_" + localStorage.getItem("currentLang");
        var filename = "";
        switch(name){

            case "itinerary":
                filename = "itenerary" + lang + ".json";
                break;
            case "destination":
                filename = "destinations" + lang + ".json";
                break;
            case "event":
                filename = "events" + lang + ".json";
                break;        
            case "activity":
                filename = "activities" + lang + ".json";
                break;   
            case "restaurant":
                filename = "restaurants" + lang + ".json";
                break;  
            case "hotel":
                filename = "accomodations" + lang + ".json";
                break;   
            case "travel":
                filename = "travel" + lang + ".json";
                break;
            case "about":
                filename = "facts" + lang + ".json";
                break; 
            case "support":
                filename = "help" + lang + ".json";
                break;        
        }
        return 'json/'+filename;
        // if(name=="itinerary") {
        //   // return AppSettings.API_ENDPOINT + "Api/CustomContent/GetAllChildren/1447";
        //   return "itenerary.json";
        // } else if(name == "destination") {
        //     return "destinations.json";
        // } else if(name == "event") {
        //     return "events.json";
        // } else if(name == "activity") {
        //     return "activities.json";    
        // } else if(name == "restaurant") {
        //     return "restaurants.json";        
        // }else if(name == "hotel") {
        //     return "accomodations.json";
        // }else {       
        //   return AppSettings.API_ENDPOINT + AppSettings.API_CONTENT_ROOT + AppSettings.API_IDS.find(item => item.DB_ENTITY == name).ENTITY_API;
        // }
        
       
    }

}
