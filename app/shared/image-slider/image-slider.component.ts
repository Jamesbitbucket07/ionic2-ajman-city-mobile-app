import {Component,Input, ViewChild} from '@angular/core';
import { Slides, Platform } from 'ionic-angular';
import { ViewController, NavParams } from 'ionic-angular';
import { DeviceOrientation, CompassHeading } from 'ionic-native';
import { TranslatePipe, TranslateService } from 'ng2-translate';

declare var screen;
@Component({
    templateUrl: 'build/shared/image-slider/image-slider.component.html',
    providers:[Slides, TranslateService],
    pipes: [TranslatePipe]
})
export class ImageSlider {
  @ViewChild('sliderImages') slider: Slides;
    images:Array<string>;
    orientation: any;
    orientationInterval: any;
    numImg: number;
    currImage: number = 1;
    showPrev: boolean = false;
    showNext: boolean = true;
   constructor(private params: NavParams, private viewCtrl: ViewController, public platform: Platform) {
       console.log(this.params.get("images"))
       var that = this;
       this.images = this.params.get("images");
       this.numImg = this.images.length;
       // this.orientationInterval = setInterval(function() {
       //   that.checkOrientation();
       // })
       window['localStorage']['deviceOrientation'] = "portrait";
    }
    ngOnInit() {
      // this.orientation = DeviceOrientation.watchHeading().subscribe(
      //   (data: CompassHeading) =>  { 
      //     if(data.magneticHeading>180&&data.magneticHeading<230 || data.magneticHeading>380 || data.magneticHeading < 20 ) {
      //       if(window['localStorage']['deviceOrientation']!="portrait") {
      //         window['localStorage']['deviceOrientation'] = "portrait"
      //         screen.unlockOrientation()
      //       }
      //     } else {
      //      if(window['localStorage']['deviceOrientation']!="landscape") {
      //         window['localStorage']['deviceOrientation'] = "landscape";
      //         screen.lockOrientation('landscape')
      //       }
      //     }
      //   }
      // );
    }
    ngOnDestroy() {
      clearInterval(this.orientationInterval);

    }
    slideNext() {
      this.slider.slideNext();
      console.log("next")
    }
    slidePrev() {
      this.slider.slidePrev();
      console.log("prev")

    }
    
    onSlideChanged(e) {
      // console.log(e);
      this.currImage = this.slider.getActiveIndex()+1;
      if(this.currImage==1) {
        this.showPrev = false;
      } else {
        this.showPrev = true;
      }
      if(this.currImage==this.images.length) {
        this.showNext = false;
      } else {
        this.showNext = true;
      }

    }

    checkOrientation() {
      console.log(this.platform.isPortrait());
    }
    close() {
        // console.log(this.hotelFilter)
       
        // screen.unlockOrientation();
 
        this.viewCtrl.dismiss();
        
    }
}