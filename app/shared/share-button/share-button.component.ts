import { Component, OnInit, Input } from '@angular/core';
import { SocialSharing } from 'ionic-native';

import {
	AppSettings
} from '../';

@Component({
	selector: 'share-button',
	templateUrl: './build/shared/share-button/share-button.component.html'
})


export class ShareButton implements OnInit {
	@Input() item: any;
	constructor() {}

	ngOnInit() {
		
	}
	openShare() {
		var title = "Take a look at Ajman!";
		var text = `Hi.
\n
Take a look at this! 
\n
`+AppSettings.SERVER_URL + this.item.url+`
\n
Download the Ajman tourism app and see why everyone's saying we're sincerely Emirati:
\n
<appstore> <googleplay>
\n
--\n
A good friend of yours has shared this information with you via the official Ajman Tourism mobile app. Please visit http://ajman.travel for more information on places to see and things to do.`;
		SocialSharing.share(text,text,"", "");
	}
}