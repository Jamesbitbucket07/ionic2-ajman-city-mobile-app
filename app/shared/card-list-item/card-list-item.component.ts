import { Component, Input, forwardRef,  } from '@angular/core';
import {NavController, ViewController, Events} from 'ionic-angular';
import * as moment from 'moment';
import { TranslatePipe } from 'ng2-translate/ng2-translate';
// import {HotelDetails} from '../../+hotels/hotel-details/hotel-details.component'
import {
  FavoriteButton,
  HotelRating,
  Hotel,
  TruncatePipe,
  TripDetails,
  HotelDetails,
  EventDetails,
  ActivityDetails,
  AttractionDetails,
  RestaurantDetails,
  StaticContent,
  TravelAgents,
  ImageSaver,
  NearbyInfo, 
  CurrencyConverter,
  SocialMedia
} from '../';

@Component({
  selector: 'card-list-item',
  templateUrl: 'build/shared/card-list-item/card-list-item.component.html',
  pipes: [TruncatePipe,TranslatePipe],
  directives:[forwardRef(() => FavoriteButton), forwardRef(() => HotelRating), forwardRef(() => ImageSaver), forwardRef(() => NearbyInfo)]
})
export class CardListItem {
    @Input() item: any;
    @Input() isNearby: boolean = false;
    @Input() coords: any;
    @Input() activeItem: any;
    showDetails = false;
    showInterface:boolean;
    constructor(public nav:NavController, private viewCtrl: ViewController, public events: Events){
     // console.log("item", this.item)
     
    }
    ngDoCheck(){
      if(this.activeItem==this.item.id) {
        this.showDetails = true;
      } else {
        this.showDetails = false;
      }
    }
    
    ngOnInit(){
      this.item.description = this.getText(this.item.description);
    }
    toggleCardDetails(){
        // this.showDetails = !this.showDetails;
        this.events.publish("cardListItem:open", this.item.id);
    }
    getDetails() {
      var detailReturn;
      if(this.item.alias=="hotel") {
        detailReturn  = HotelDetails;
      }
      if(this.item.alias=="itinerary") {
        detailReturn = TripDetails;
      }
      if(this.item.alias=="destination") {
        detailReturn = AttractionDetails;
      }
      if(this.item.alias=="event") {
        detailReturn = EventDetails;
      }
      if(this.item.alias=="activity") {
        detailReturn = ActivityDetails;
      }
      if(this.item.alias=="restaurant") {
        detailReturn = RestaurantDetails;
      }
      if(this.item.alias=="static") {
        detailReturn = StaticContent;
      }
      if(this.item.alias=="travelAgents") {
        detailReturn = TravelAgents;
      }
      if(this.item.id==3474 || this.item.id==3480) {
        detailReturn = CurrencyConverter;
      }
      if(this.item.id==3475 || this.item.id==3483) {
        detailReturn = SocialMedia;
      }
      return detailReturn;
    }
    private formatDate(date:string){
      return moment(date).format('DD/MM/YYYY');
    }
    private showDetailsPage(){
          this.nav.push(this.getDetails(),{
            item: this.item,
            popbutton: true
          });
          // this.nav.push(HotelDetails,{
          //   item: this.item,
          //   popbutton: true
          // });
    }
    getText(str) {
      try{
        return str.replace(/<[^>]+>/ig, "");  
      } catch(err) {
        // console.log("getText", str, err);
        return "";
      }
      
    }
    
}
