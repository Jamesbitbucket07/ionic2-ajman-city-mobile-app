import {Component, ChangeDetectorRef} from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import {App} from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { TranslatePipe, TranslateService, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { LanguageService } from "../services/language.service";
import { SocialSharing } from 'ionic-native';

@Component({
    templateUrl: 'build/shared/share-popover/share-popover.component.html',
    providers: [TranslateService, TranslateLoader, LanguageService,{ 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    }],
    pipes: [TranslatePipe]
})
export class SharePopover {
    
    item:any;
    filterForm:any;

    constructor(public translateService:TranslateService,private params: NavParams, private viewCtrl: ViewController, public ref: ChangeDetectorRef) {
        this.item = this.params.get("item");
        this.filterForm = this.params.get("filter");
    }
    ngOnInit() {
        // console.log("filter", this.params.get("filter"))
        // this.filterForm = this.params.get("filter");
        this.translateService.use(window['localStorage']['currentLang']);
    }
    save() {
        this.viewCtrl.dismiss();
    }
    close() {
        this.viewCtrl.dismiss();
    }
    changeFilter(evt, type) {
        // console.log(type)
        // this.filterForm[type].checked = evt.target.checked;
        // this.ref.detectChanges();
    }
    sharePage(destination) {
        var that = this;
        if(destination=="facebook") {
           SocialSharing.shareViaFacebook(this.item.name,this.item.substitle,""); 
        }
        if(destination=="email") {
            SocialSharing.share(this.item.name,this.item.substitle,"","");
        }
        
        if(destination=="twitter") {
            SocialSharing.shareViaTwitter(this.item.name+" "+this.item.substitle,"")
        }
    }
    isChecked(item) {
        return this.filterForm[item].checked;
    }
}