import { Component, OnInit, Input } from '@angular/core';
import { DistanceCalculator } from '../helpers/DistanceCalculator';

@Component({
	selector: 'nearby-info',
	templateUrl: './build/shared/nearby-info/nearby-info.component.html'
})
export class NearbyInfo implements OnInit {
	@Input() coords: any = null;
	@Input() item: any;
	@Input() text: string;
	distance: string;
	constructor() {


	}

	ngOnInit() {
		if(this.coords) {
			this.distance = this.calculateDistance(this.item, this.coords, "km");	
		}
		
	}

	getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
	    var R = 6371; // Radius of the earth in km
	    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
	    var dLon = this.deg2rad(lon2 - lon1);
	    var a =
	        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
	        Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
	        Math.sin(dLon / 2) * Math.sin(dLon / 2);
	    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    var d = R * c; // Distance in km
	    return d;
	  }

	 deg2rad(deg) {
	    return deg * (Math.PI / 180)
	  }
	  calculateDistance(element:any, coords: any, unit: string) {
	  	var d = this.getDistanceFromLatLonInKm(parseFloat(coords.latitude), parseFloat(coords.longitude), element.latitude, element.longitude);
	  	if(1>d) {
	  		return Math.round(d*1000) + "m";
	  	} else {
	  		return Math.round(d) + "km"
	  	}
	  	
	  }
}