import { Component } from '@angular/core';
import { MenuController, Events  } from 'ionic-angular';

@Component({
  selector: 'toggle-nav',
  templateUrl: 'build/shared/toggle-nav/toggle-nav.component.html'
})

export class ToggleNav {

  constructor(private menu: MenuController, public events: Events) {
  }

  openMenu(elm) {
    this.events.publish("camera:close", true);
    document.getElementById("container-box").classList.add("menu-content-open");
    this.menu.open();
  }
}
