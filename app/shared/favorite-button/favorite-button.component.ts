import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';

import {
    Favorites
} from '../';

@Component({
    selector: 'favorite-button',
    templateUrl: 'build/shared/favorite-button/favorite-button.component.html',
    providers: [Favorites],
    styles: []
})
export class FavoriteButton {
    @Input() item: any;
    constructor(public favorites: Favorites, public events: Events) {

    }
    ngOnInit() {
        this.item.favorite = this.favorites.itemIsFavorite(this.item.id);

    }
    toggleFavorite() {
        if (this.favorites.itemIsFavorite(this.item.id)) {
            this.item.favorite = false;
            this.favorites.removeFromFavorites(this.item.id);
        }
        else {
            this.item.favorite = true;
            this.favorites.addToFavorites(this.item.id);
        }
        this.events.publish("favourites:change", true);
    }
}