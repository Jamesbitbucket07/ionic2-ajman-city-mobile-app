import { Component, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { TranslatePipe } from 'ng2-translate/ng2-translate';
import { Platform, Events, MenuController } from 'ionic-angular';
import { SocialSharing, Base64ToGallery } from 'ionic-native';

import { 
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar
} from '../shared'

declare var cordova;
declare var Cropper;
declare var CameraPreview;

@Component({
  selector: "camera",
  templateUrl: 'build/camera/camera.component.html',
  pipes: [TranslatePipe],
  directives: [TopBar]
  
})

export class CameraFrame {
	@ViewChild('imageSrc') input: ElementRef;
	@ViewChild('cropped') cropped: ElementRef;
	@ViewChild('canvas2Save') canvasToSave: ElementRef;

	public base64Image: string;
	platform: any;
	cameraActive = true;
	cameraFront: boolean = false;
	imgCamera: any = null;
	hideImgCamera: boolean = false;
	imgCropped: any = null;
	cropper: any;
	joiner: any;
	frames: any;
	selectedFrame: any = null;
	shareWindow: boolean = false;
	shareButton: boolean = false;
	flashStatus: boolean = false;
	imgHeight: number = 0;
	constructor(platform: Platform, private ref: ChangeDetectorRef, public events: Events, public menu: MenuController) {
		this.joiner = window['photojoiner'];
		this.events.publish("camera:open", true);
		this.platform = platform;

		console.log(this.menu)
		this.menu.swipeEnable(false);
		this.frames = [
		{	"frame": "imgs/photo-frames/photo-frame-1.png" },
		{	"frame": "imgs/photo-frames/photo-frame-2.png" },
		{	"frame": "imgs/photo-frames/photo-frame-3.png" },
		{	"frame": "imgs/photo-frames/photo-frame-4.png" },
		{	"frame": "imgs/photo-frames/photo-frame-5.png" },
		{	"frame": "imgs/photo-frames/photo-frame-6.png" },
		{	"frame": "imgs/photo-frames/photo-frame-7.png" },
		{	"frame": "imgs/photo-frames/photo-frame-8.png" },
		{	"frame": "imgs/photo-frames/photo-frame-9.png" },
		{	"frame": "imgs/photo-frames/photo-frame-10.png" },
		{	"frame": "imgs/photo-frames/photo-frame-11.png" },
		];
	}

	ionViewDidEnter() {
		var that = this;
		
			that.openCamera();
		
		
	}
	ngOnDestroy() {
		CameraPreview.stopCamera();
		this.menu.swipeEnable(true);
	}
	ionViewDidLeave() {
		this.events.publish("camera:close", true);

	}

	openCamera() {
		var that = this;
		this.platform.ready().then(() => {
			that.imgHeight = this.platform.width();
			console.log(that.imgHeight)
	    	let tapEnabled = false;
	        let dragEnabled = true;
	        let toBack = true;
	        this.cameraActive = true;
	        let rect = {
	            x: 0, 
	            y: 70, 
	            width: this.platform.width(), 
	            height: this.platform.width(),
	            camera: 'back',
				tapPhoto: false,
				previewDrag: false,
				toBack: true,
				alpha: 0
	        };
	        
	        CameraPreview.setOnPictureTakenHandler(function(result){
	        	
				CameraPreview.hide();
				if(that.imgCamera == null) {
					// that.input.nativeElement.src = result;
						// if(that.cameraFront) {
						// 	that.cameraToBase64(result)
						// } else {
							var fixUrl = "";
							if(that.platform.is("android")) {
								fixUrl = "data:image/jpeg;base64,";
							}
							that.imgCamera = fixUrl+result;
							that.cameraActive = false;
							that.ref.detectChanges();
						// }
					
				}
				
			});
	 		setTimeout(function() {
	        	CameraPreview.startCamera(rect, function(s) { CameraPreview.show() }, function(e) { console.log(e)});
	        }, 500);

    	})
	}

	selectFrame(item) {
		this.selectedFrame = item;
		this.ref.detectChanges();
	}


	imageLoaded() {
	    this.cropper = new Cropper(this.input.nativeElement, {
	    	aspectRatio: 1/1,
	        viewMode: 3,
	        dragMode: 'move',
	        autoCropArea: 1,
	        movable: false,
	        restore: true,
	        modal: false,
	        guides: false,
	        center: false,
	        highlight: false,
	        cropBoxMovable: false,
	        cropBoxResizable: false,
	        crop: (e) => {
	        	console.log("crop", e)
	        },
	        built: (e) => {
	        	console.log("built", e);
	        }
	    });
	}

	cameraToBase64(url) {
		var img = new Image();
	    img.crossOrigin = 'Anonymous';
	    var that = this;
	    img.onload = function(){
	        var canvas = document.createElement('canvas');
	        var ctx = canvas.getContext('2d');
	        canvas.height = this.height;
	        canvas.width = this.width;
	        console.log(this.src)
	        ctx.rotate(180 * Math.PI / 180);
	        ctx.drawImage(this,0,0);
	        var dataURL = canvas.toDataURL('image/png');
	        that.returnImage(dataURL);

	        canvas = null; 
	    };
	    img.src=url
	}

	returnImage(img) {
		console.log(img);
		this.imgCamera = img;
		this.cameraActive = false;
		this.ref.detectChanges();
	}

	convertImgToBase64(){
		var newURL = this.selectedFrame.frame;
	    var img = new Image();
	    img.crossOrigin = 'Anonymous';
	    var that = this;
	    img.onload = function(){
	        var canvas = document.createElement('canvas');
	        var ctx = canvas.getContext('2d');
	        canvas.height = this.height;
	        canvas.width = this.width;
	        ctx.drawImage(this,0,0);
	        var dataURL = canvas.toDataURL('image/png');
	        that.cropCamera(dataURL);

	        canvas = null; 
	    };
	    img.src = newURL;
	    
	}
  

	takePicture(){
		CameraPreview.takePicture({maxWidth:this.platform.width(), maxHeight:10000});
  	}
  	cropCamera(frame){
		// cordova.plugins.camerapreview.switchCamera();
		var img = this.cropper.getCroppedCanvas({
		  width: this.platform.width(),
		  height: this.platform.width()
		});

		console.log(this.cropper);

		// this.imgCamera = null;
		delete(this.imgCamera);

		console.log(img);
		var imgCrop = img.toDataURL();
		var that = this;
		// console.log("firstImage", imgCrop);
		// console.log("secondImage", frame);
		

		// window['plugins'].JoinImages.join({
		// 	firstImage: imgCrop.replace("data:image/png;base64,",""),
		// 	secondImage: frame.replace("data:image/png;base64,",""),
		// 	success: function(encodedImage) {
		// 		 "data:image/png;base64,"+encodedImage
		// 	},
		// 	error: function(message) {
		// 		console.error(message);
		// 	}
		// })
		that.imgCropped = true;
		// that.shareButton = true;
		// that.selectedFrame = null;
		this.switchShare();
		
		that.joiner.join({
			'images': [imgCrop, frame],
			'canvasHeight': this.platform.width()
		});

		that.ref.detectChanges();
		
	}
	switchCamera(){
		CameraPreview.switchCamera();
		this.cameraFront = (this.cameraFront)?false:true;
	}
	switchFlash(){
		if(this.flashStatus) {
			this.flashStatus = true;
			CameraPreview.setFlashMode(1);
		} else {
			this.flashStatus = false;
			CameraPreview.setFlashMode(0);	
		}
		
	}
	openShareWindow() {
		this.shareWindow = true;
		this.ref.detectChanges();
	}

	resetCamera() {
		this.selectedFrame = null;
		this.shareWindow = false;
		this.shareButton = false;
		this.flashStatus = false;
		this.imgCamera = null;
		this.hideImgCamera = false;
		this.imgCropped = null;
		var ctx = this.canvasToSave.nativeElement.getContext('2d');
		ctx.clearRect(0, 0, this.platform.width(), this.platform.width());
		this.cropper.destroy();
		CameraPreview.stopCamera();
		this.ref.detectChanges();
		this.openCamera();
	}
	stripUrl(url) {
		var filename;
		if(this.platform.is("android")) {
			var path = cordova.file.externalRootDirectory
			var r = url.split("/");
			filename = path + "/Pictures/"+r[r.length-1];
		} else {
			filename = url;
		}
		return filename;
	}

	shareImage(destination) {
		var that = this;
		var filename;
		if(destination=="facebook") {
			var dataURL = this.canvasToSave.nativeElement.toDataURL();

			this.saveImage().then(res => {
				filename = this.stripUrl(res);
				SocialSharing.shareViaFacebook(filename,filename,"");
			},
			err => {})
			
		}
		if(destination=="share") {

			this.saveImage().then(res => {
				console.log("gallery", res);
				filename = this.stripUrl(res);
				SocialSharing.share("","",filename,"");
			},
			err => {})
			
		}
		if(destination=="instagram") {
			this.saveImage().then(res => { 
				filename = this.stripUrl(res);
				SocialSharing.shareViaInstagram("",filename)
			},
			err => {})
		}
		if(destination=="twitter") {
			this.saveImage().then(res => {
				filename = this.stripUrl(res);
				SocialSharing.shareViaTwitter("",filename)
			},
			err => {})
		}
		if(destination=="album") {
			this.saveImage().then(res => {
				filename = this.stripUrl(res);
				SocialSharing.share("","",filename,"");
			},
			err => {})
		} 
	}
	switchShare() {
		if(this.shareWindow) {
			this.shareWindow = false;
		} else {
			this.shareWindow = true
		}
		this.ref.detectChanges();
	}


	saveImage() {
        var dataURL = this.canvasToSave.nativeElement.toDataURL();
        console.log({"element": this.canvasToSave, "url": dataURL})
        return Base64ToGallery.base64ToGallery(dataURL, 'img_');
	}
}