import { Component, forwardRef,  } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import {
  AuthorizationService,
  DbService,
  HttpClient,
  ImageCache,
  ContentService,
  CardList,
  TopBar,
  ConnectivityService
} from '../shared'

@Component({
  templateUrl: 'build/+hotels/hotels.component.html',
  providers: [
    DbService,
    AuthorizationService,
    HttpClient,
    ImageCache,
    ContentService,
    ConnectivityService
  ],
  directives: [forwardRef(() => TopBar), forwardRef(() => CardList)] //, forwardRef(() => AjmanMap)]
})
export class Hotels {
  didLoad: boolean;
  loading: any;
  filterFields:any;
  filterValues:any;
  sort:any;
  scrollBar: boolean = false;
  constructor(public loadingCtrl: LoadingController,public contentService: ContentService){

    this.filterValues = [
      { field:"rating", value: 14, checked: false},
      { field:"rating", value: 15, checked: false},
      { field:"rating", value: 16, checked: false},
      { field:"rating", value: 17, checked: false},
      { field:"rating", value: 18, checked: false},
      { field:"hotelApartmentsRating", value: 229, checked: false},
      { field:"hotelApartmentsRating", value: 230, checked: false}
    ]
   
    this.filterFields = ['rating','hotelApartmentsRating','sortOrder'];
    
    

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: false
    });
    this.loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
    //this.presentLoading();

  }
  presentLoading() {
    this.loading.present();
  }
  scrolled(value) {
    this.scrollBar = value;
  }
  dismissLoading() {
    // this.loading.dismiss();
  }
  // allHotels: Array<any>;
  // filteredHotels: Array<any>;
  // limit: number;
  // lat: number;
  // lng: number;
  // currentPage: number;
  // constructor(public ContentService: ContentService, public imageCache: ImageCache) {
  //   this.limit = 10;
  //   this.currentPage = 0;
  //   this.filteredHotels = new Array<any>();
  //   this.getHotels();
  // }

  // getMoreHotels() {
    
  //   return new Promise(resolve => {
  //     resolve(this.allHotels[this.currentPage]);
  //     this.currentPage++;
  //     console.log(this.currentPage)
  //   });

  // }


  // private getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  //   var R = 6371; // Radius of the earth in km
  //   var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
  //   var dLon = this.deg2rad(lon2 - lon1);
  //   var a =
  //     Math.sin(dLat / 2) * Math.sin(dLat / 2) +
  //     Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
  //     Math.sin(dLon / 2) * Math.sin(dLon / 2);
  //   var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  //   var d = R * c; // Distance in km
  //   return d;
  // }

  // private deg2rad(deg) {
  //   return deg * (Math.PI / 180)
  // }

  // private getHotels() {

  //   var items = new Array<Hotel>();
  //   var that = this;
    // if (this.allHotels) {
    //   this.getMoreHotels().then((res) => this.filteredHotels = concat(this.filteredHotels, res as any[]));
    // }
    // else {

  //     Geolocation.getCurrentPosition().then(pos => {
  //       this.lat = pos.coords.latitude;
  //       this.lng = pos.coords.longitude;
  //       var coords = { latitude: this.lat, longitude: this.lng };
  //       that.ContentService.getAll(Hotel, 'hotel')
  //         .subscribe(res => {
  //           that.calculateDistance(res, coords);
  //           this.getMoreHotels().then((res) => this.filteredHotels = concat(this.filteredHotels, res as any[]));
  //         });
  //     });
  //   //}

  // }
 
  // private calculateDistance(results: Array<any>, coords: any) {
  //   for (var index = 0; index < results.length; index++) {
  //     var element = results[index];
  //     var distance = Math.round(this.getDistanceFromLatLonInKm(parseFloat(coords.latitude), parseFloat(coords.longitude), element.latitude, element.longitude) * 1000);
  //     element.distance = distance;
  //   }
  //   this.allHotels = chunk(this.sortHotelsByDistance(results), this.limit);
  // }
  // private sortHotelsByDistance(res) {
  //   return res.sort(function (a: any, b: any) {
  //     if (a.distance < b.distance) return -1;
  //     if (a.distance > b.distance) return 1;
  //     return 0;
  //   });
  // }

  // doRefresh(infiniteScroll) {
   
  //   this.getMoreHotels().then(res => {
  //     console.log("res",res)
  //     this.filteredHotels = concat(this.filteredHotels, res as any[])
  //         infiniteScroll.complete();
  //   });
  // }

}
