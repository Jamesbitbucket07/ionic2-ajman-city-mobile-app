import { ImageCache, AppSettings } from '../../shared';


/**
 * Hotel Enums
 */
export enum HotelType {
    Hotel = 195,
    Motel = 224,
    HotelApartment = 196,
    FurnishedApartment = 223,
    Resort = 222
}

export enum HotelApartmentRating {
    Standard = 229,
    Premium = 230
}
export enum HotelFacilities{
    WiFi_Internet = 298,
    Twenty_Four_Hour_Front_Desk = 299,
    Tour_Desk = 300,
    Baggage_Storage = 301,
    Concierge_Service = 302,
    ATM_Onsite = 303,
    Currency_Exchange = 304,
    Storage_Room = 305,
    Valet_Parking = 306,
    Free_Parking = 307,
    Wheelchair_Access = 308,
    Laundry = 309,
    Safety_Deposit_Box = 310,
    Room_Service = 311,
    Spa = 312,
    Sauna = 313,
    Indoor_Pool = 314,
    Outdoor_Pool = 315,
    Meeting_Banquet_Facilities = 316,
    Business_Centre = 317,
    Fitness_Centre = 318,
    Activity_Centre = 319,
    Kids_Club = 320,
    Restaurants_and_Cafes = 321,
    Retail = 322,
    Pet_Friendly = 323
}
/**
 * Hotel Class
 */
export class Hotel {
   id:number = null; 
   searchIndex:string = null; 
   _id:string = null; 
   name: string = null;
   email: string = null;
   location: string = null;
   latitude: number = null;
   longitude: number = null;
   phone: string = null;
   website: string = null;
   streetAddress: string = null;
   type: HotelType = null;
   rating: string = null;
   tripAdvisorId: string = null;
   facilitiesAndFeatures: Array<HotelFacilities> = null; 
   roomPrice: string = null;
   bookingUrl: string = null;
   mapContent: string = null;
   description: string = null;
   featured: boolean = false;
   hotelApartmentsRating: HotelApartmentRating = null;
   hotelImages: Array<string> = null;
   hotelLogo: string = null;
   featuredImage: string = null;
   bannerImage: string = null;
    url: string = null;
   cardImage: string = null;
   destinations: Array<number> = null;
   activities: Array<number> = null;
   updateDate: Date = null;
   favorite: boolean = false;
   alias:string = null;
   icon:string = './imgs/pins/pin-accommodation-hd.png'; 
   sortOrder:number = null;



   public getInfo(id, type){
    var obj = {
       "roomPrice" : {
          255: "0-250",
          226: "250-500",
          227: "500-1000",
          228: "1000+"
         },
         "type": {
           222: "Resort",
           195: "Hotel",
           224: "Hotel Apartment",
           196: "Apartment",
           223: "Furnished Apartment"
         }
       }
     return obj[type][id];
   }

   public static getHotelFacilitiesLabel(val:number)
   {
       return HotelFacilities[val]; 
   }

  public static getFilterStatement(filterValues:any)
   {
       var orStatement = [];
        
        if(filterValues.oneStar)
            orStatement.push({rating:{$eq:"14"}});
        if(filterValues.twoStar)
            orStatement.push({rating:{$eq:"15"}});
        if(filterValues.threeStar)
            orStatement.push({rating:{$eq:"16"}});
        if(filterValues.fourStar)
            orStatement.push({rating:{$eq:"17"}});
        if(filterValues.fiveStar)
            orStatement.push({rating:{$eq:"18"}});       

        if(filterValues.premium)
            orStatement.push({hotelApartmentsRating:{$eq:"230"}});       

        if(filterValues.standard)
            orStatement.push({hotelApartmentsRating:{$eq:"229"}});   


             
       
        var filter = {_id:{$gte:0},$or:orStatement};


        return filter;
   }


   constructor(item: any, public imageCache)
   {

       if (item) {

            for (var field in item) {
                if (!this.hasOwnProperty(field) || !item[field]) continue;
                this[field] = item[field];
            }
            var allValues = item["properties"]["$values"];
            
            for (var i = 0; i < allValues.length; i++) {
                if (!this.hasOwnProperty(allValues[i]["Alias"]) || !allValues[i]["Value"]) continue;

                this[allValues[i]["Alias"]] = allValues[i]["Value"];

                 if(allValues[i]["Alias"] === 'roomPrice'){
                   this[allValues[i]["Alias"]] = this.getInfo(allValues[i]["Value"], allValues[i]["Alias"]);
                 }
                
                if(allValues[i]["Alias"] === 'hotelImages'){
                    
                    this.hotelImages = allValues[i]["Value"].split(',').map(item => {
                        var fileName = item.split('//').pop().split('/').pop();
                        return "media/hotel/" +  fileName;
                    });
                }


                if(allValues[i]["Alias"] === "facilitiesAndFeatures" && allValues[i]["Value"])
                    this.facilitiesAndFeatures = allValues[i]["Value"].split(",");
                    
            }

            if (!this.featuredImage) {
                if(this.bannerImage){
                    var fileName = this.bannerImage.split('//').pop().split('/').pop();
                    this.cardImage = "media/hotel/" +  fileName;
                }
            } else {
                var fileName = this.featuredImage.split('//').pop().split('/').pop();
                this.cardImage = "media/hotel/" +  fileName;
                this.featuredImage = this.cardImage;
                
            }

            if (this.location) {
                var l = this.location.split(',');
                this.latitude = parseFloat(l[0]);
                this.longitude = parseFloat(l[1]);

            }
            // console.log(item, this);

        }
       
   }
}
