import { Component, Input } from '@angular/core';
import { TranslatePipe } from 'ng2-translate/ng2-translate';

import {
  Hotel
} from '../../shared';

@Component({
  selector: 'hotel-rating',
  templateUrl: 'build/+hotels/shared/hotel-rating/hotel-rating.component.html',
  pipes:[TranslatePipe],
  styles:[`
    
  `]
})

export class HotelRating {
    @Input() item: any;
   
    constructor(){
   
    }
  
}
