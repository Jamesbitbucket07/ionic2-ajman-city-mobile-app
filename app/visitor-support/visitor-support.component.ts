import { Component, OnInit, forwardRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import {
	ContentService,
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar,
  	CardList,
  	DbService,
    HttpClient,
    AuthorizationService,
    ImageCache
} from '../shared'


@Component({
	templateUrl: 'build/visitor-support/visitor-support.component.html',
	providers: [
    ContentService,
    DbService,
    HttpClient,
    AuthorizationService,
    ImageCache,
  ],
  directives: [TopBar, forwardRef(() => CardList)]
})
export class VisitorSupport implements OnInit {

	items: any = null;
	loading: any;
	didLoad: any;
	scrollBar: boolean = false;
	constructor(public loadingCtrl: LoadingController, private dbService: DbService) {
		this.loading = this.loadingCtrl.create({
	      content: "Please wait...",      
	      duration: 2000,
	      dismissOnPageChange: false
	    });
	    this.loading.onDidDismiss(()=>{
	      this.didLoad = true;
	    });
	    //this.presentLoading();
		}

	ngOnInit() {
		var that = this;
		var parent = localStorage.getItem("currentLang") == "en" ? [3436] : [3479];
		this.dbService.getFiltered({id: {$in : parent}}, [], ["id"], 0, 30).subscribe(res => {
	    	var items = res.items[0]['childrenPages'];
	    	that.items = items;
	    })
	}
	presentLoading() {
      this.loading.present();
    }
    dismissLoading() {
      //this.loading.dismiss();
    }
    scrolled(value) {
    this.scrollBar = value;
  }
}