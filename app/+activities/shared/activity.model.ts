import { ImageCache } from '../../shared/services/image-cache.service';
import { AppSettings } from '../../shared/services/app-settings.service';

/**
 * Activity Categories
 */
export enum ActivityCategory {
    Beach = 0,
    Urban = 1,
    Mountain = 2,
    Mangroves = 3,
    Nature_And_Wild_Life = 653,
    Spa = 5,
    Heritage = 655,
    Dining = 7,
    Art_And_Culture = 8,
    Walking_And_Hiking = 658,
    Mountain_Biking = 659,
    Water_Sports = 11,
    Golf = 661,
    Desert_Safari = 662,
    Sightseeing = 14
}

/**
 * Activity Class
 */
export class Activity {
    id: number = null;
    searchIndex:string = null; 
    _id: string = null
    name: string = null;
    location: string = null;
    latitude: number = null;
    longitude: number = null;
    description: string = null;
    subtitle: string = null;
    bannerImage: string = null;
    featuredImage: string = null;
    cardImage: string = null;
    updateDate: Date = null;
    images: Array<string> = null;
    mapContent: string = null;
    category: ActivityCategory = null;
    keyInfo: string = null;
    destinations: Array<number> = null;
    tripAdvisorId: string = null;
    featured: Boolean = false;
    favorite: boolean = false;
    alias: string = null;
    url: string = null;
    icon: string = './imgs/pins/pin-todo-hd.png';
    sortOrder:number = null;
//     public static getFilterStatement(filterValues:any)
//    {
//        var orStatement = [];
        
//         if(filterValues.beach)
//             orStatement.push({category:{$eq:"659"}});
//         if(filterValues.culture)
//             orStatement.push({category:{$eq:"655"}});
//         if(filterValues.cycling)
//             orStatement.push({category:{$eq:"16"}});
//         if(filterValues.desert)
//             orStatement.push({category:{$eq:"662"}});
//         if(filterValues.golf)
//             orStatement.push({category:{$eq:"661"}});       
//         if(filterValues.scenic)
//             orStatement.push({category:{$eq:"653"}});
//         if(filterValues.shopping)
//             orStatement.push({category:{$eq:"15"}});
//         if(filterValues.spa)
//             orStatement.push({category:{$eq:"16"}});
//         if(filterValues.sports)
//             orStatement.push({category:{$eq:"17"}});
//         if(filterValues.watersports)
//             orStatement.push({category:{$eq:"18"}});     

//         var filter = { $and:[{alias: { $eq: 'activity' }},{$or:orStatement}] };


//         return filter;
//    }

    constructor(item: any, public imageCache) {
        if (item) {
            for (var field in item) {
                if (!this.hasOwnProperty(field) || !item[field]) continue;


                this[field] = item[field];
            }
            var allValues = item["properties"]["$values"];
            for (var i = 0; i < allValues.length; i++) {
                if (!this.hasOwnProperty(allValues[i]["Alias"]) || !allValues[i]["Value"]) continue;
                
                if(allValues[i]["Alias"] === 'images'){
                    
                    this.images = allValues[i]["Value"].split(',').map(item => {
                        var fileName = item.split('//').pop().split('/').pop();
                        return "media/activity/" +  fileName;
                    });
                }
                else    
                    this[allValues[i]["Alias"]] = allValues[i]["Value"];
            }

            if (!this.featuredImage) {
                if(this.bannerImage){
                    var fileName = this.bannerImage.split('//').pop().split('/').pop();
                    this.cardImage = "media/activity/" +  fileName;
                    this.featuredImage = "media/activity/" +  fileName;
                }
            } else {
                var fileName = this.featuredImage.split('//').pop().split('/').pop();
                this.cardImage = "media/activity/" +  fileName;
                this.featuredImage = this.cardImage;
                this.bannerImage = this.cardImage;
                
            }

            if (this.location) {
                var l = this.location.split(',');
                this.latitude = parseFloat(l[0]);
                this.longitude = parseFloat(l[1]);

            }
            
        }
    }


}
