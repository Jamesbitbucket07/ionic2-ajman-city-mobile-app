import { Component } from '@angular/core';
import { TranslatePipe } from 'ng2-translate/ng2-translate';

import { 
	Weather,
    PrayerTimes
} from '../../shared';

import '../../shared/helpers/extensions';


@Component({
    selector: 'home-widgets',
    templateUrl: 'build/home/home-widgets/home-widgets.component.html',
    providers: [Weather, PrayerTimes],
    pipes: [TranslatePipe]
})


export class HomeWidgets {
    currentTemp: any = {
        code: "",
        date: "",
        temp: "",
        text: "",
        icon: "sunny.svg"
    };
    currentPrayers: any = { name: '', time: '' };
    conditions: Array<string>;
    showWeather: boolean;
    showPrayer: boolean;
    showTime: boolean;
    timeoutHandler: any;
    currentView: string = '';
    currentTime: string;
    constructor(public weather: Weather, public prayers: PrayerTimes) {

        //this.startWidgets();
        this.conditions = [];
        this.conditions[0] = "wi-tornado";
        this.conditions[1] = "wi-storm-warning";
        this.conditions[2] = "wi-hurricane";
        this.conditions[3] = "wi-thunderstorm";
        this.conditions[4] = "wi-thunderstorm";
        this.conditions[5] = "wi-rain-mix";
        this.conditions[6] = "wi-sleet";
        this.conditions[7] = "wi-rain-mix";
        this.conditions[8] = "wi-sprinkle";
        this.conditions[9] = "wi-sprinkle";
        this.conditions[10] = "wi-rain-mix";
        this.conditions[11] = "wi-showers";
        this.conditions[12] = "wi-showers";
        this.conditions[13] = "wi-snowflake-cold";
        this.conditions[14] = "wi-snow";
        this.conditions[15] = "wi-snow-wind";
        this.conditions[16] = "wi-snow";
        this.conditions[17] = "wi-hail";
        this.conditions[18] = "wi-sleet";
        this.conditions[19] = "wi-dust";
        this.conditions[20] = "wi-fog";
        this.conditions[21] = "wi-windy";
        this.conditions[22] = "wi-smoke";
        this.conditions[23] = "wi-strong-wind";
        this.conditions[24] = "wi-windy";
        this.conditions[25] = "wi-snowflake-cold";
        this.conditions[26] = "wi-cloudy";
        this.conditions[27] = "wi-night-cloudy-high";
        this.conditions[28] = "wi-day-cloudy-high";
        this.conditions[29] = "wi-night-alt-cloudy";
        this.conditions[30] = "wi-day-cloudy";
        this.conditions[31] = "wi-night-clear";
        this.conditions[32] = "wi-day-sunny";
        this.conditions[33] = "wi-night-alt-partly-cloudy";
        this.conditions[34] = "wi-day-sunny";
        this.conditions[35] = "wi-rain-mix";
        this.conditions[36] = "wi-hot";
        this.conditions[37] = "wi-thunderstorm";
        this.conditions[38] = "wi-thunderstorm";
        this.conditions[39] = "wi-thunderstorm";
        this.conditions[40] = "wi-rain";
        this.conditions[41] = "wi-snow";
        this.conditions[42] = "wi-rain-mix";
        this.conditions[43] = "wi-cloud";
        this.conditions[44] = "wi-thunderstorm";
        this.conditions[45] = "wi-hail";
        this.conditions[46] = "wi-showers"
        this.conditions[47] = "wi-thunderstorm";
    }
    stopWidgets(){
        clearTimeout(this.timeoutHandler);
    }
    getLocalDate(offset: number, date?: Date) {
        var d = date;
        if (!date)
            d = new Date();
        var localTime = d.getTime();
        var localOffset = d.getTimezoneOffset() * 60000;
        var utc = localTime + localOffset;
        var nd = new Date(utc + (3600000 * offset));
        return nd;
    }
    calcTime(offset) {
        var localDate = this.getLocalDate(offset);
        var h = localDate.getHours();
        var hh = (h < 10 ? '0' : '') + h;
        var period = localDate.getHours() >= 12 ? ' PM' : ' AM';
        var m = (localDate.getMinutes() < 10 ? ':0' : ':') + localDate.getMinutes();
        return hh + m;

    }
    startWidgets() {
        if (this.currentView === '' || this.currentView === 'time') {
            this.weather.getWeather()
                .subscribe(w => {
                    var temp = w.json();
                    this.currentTemp = temp.query.results.channel.item.condition;
                    this.currentTemp.icon = temp.query.results.channel.item.condition.code + '.svg';
                    this.currentTemp.condition = this.conditions[temp.query.results.channel.item.condition.code];
                    this.currentView = 'weather';
                },
                err => console.log(err));
        }
        else if (this.currentView === 'weather') {
            this.prayers.getPrayerTimes()
                .subscribe(p => {
                    var milliseconds = new Date().getTime();
                    var todayPrayers = p.data.timings;
                    var currentDate = this.getLocalDate(4);
                    // console.log(p)
                    for (var k in todayPrayers) {
                        if (k == "Midnight" || k == "Sunrise" || k == "Sunset") continue;
                        if (todayPrayers.hasOwnProperty(k)) {
                            var date = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), parseInt(todayPrayers[k].split(':')[0]), parseInt(todayPrayers[k].split(':')[1]), 0, 0);
                            if (date >= currentDate) {
                                this.currentPrayers.name = k;
                                this.currentPrayers.time = todayPrayers[k];
                                break;
                            }
                        }
                    }
                    if(this.currentPrayers.time == ''){
                        for (var k in todayPrayers) {
                            if (k == "Fajr") {
                                this.currentPrayers.name = k;
                                this.currentPrayers.time = todayPrayers[k];
                                break;
                            }
                        }
                    }
                    this.currentView = 'prayer';
                },
                err => console.log(err));
        }
        else if (this.currentView === 'prayer') {
            this.currentTime = this.calcTime(4);
            this.currentView = 'time'
        }
        else {
            this.currentView = 'weather'
        }
        clearTimeout(this.timeoutHandler);
        this.timeoutHandler = setTimeout(() => {
            this.startWidgets();
        }, 5000);
    }

}