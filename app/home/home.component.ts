import { Component, ViewChild, forwardRef, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NavController, MenuController, Slides, ModalController, Events } from 'ionic-angular';
import { TranslatePipe } from 'ng2-translate/ng2-translate';
import { Splashscreen } from 'ionic-native';

import {
	VisitorSupport,
	ContentService,
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar,
	NearBy,
	StaticContent
} from '../shared'

import '../shared/helpers/extensions';


declare var CameraPreview;
@Component({
  templateUrl: 'build/home/home.component.html',
  directives: [TopBar, Intro, forwardRef(() => HomeWidgets) ],
  providers: [LanguageService, Slides],
  pipes: [TranslatePipe]
})

export class HomePage {
	@ViewChild(forwardRef(() => HomeWidgets)) hw: HomeWidgets;
	@ViewChild("videoBg") videoBg: ElementRef;
	shortcuts: any;
	links: any;
	language: any;
	firstTime: boolean = true;
	menuCtrl: any;
	videoNumber: any;
	showIntro: boolean = false;
	constructor(public events: Events, private nav: NavController, private menu: MenuController, language: LanguageService, private ref: ChangeDetectorRef, private contentService: ContentService) {
		
		this.language = language;
		this.menuCtrl = menu;
		this.videoNumber = this.getRandom(1, 10);
	}

	ngOnInit() {
		
		this.shortcuts = [
			{
				"page": NearBy,
				"icon": "primary-nearby",
				"title": "Nearby Attractions",
			},
			{
				"page": Activities,
				"icon": "primary-water-sports",
				"title": "Things<br>To Do"
			},
			{
				"name": "Places<br>To Go",
				"page": Restaurants,
				"icon": "primary-dining",
				"title": "Places<br>To Eat"
			},
			{
				"page": Hotels,
				"icon": "primary-accommodation",
				"title": "Find<br>Accommodation"
			}
		];
		this.links = [
			{
				"page": null,
				"icon": "mic",
				"title": "Search<br>",
				"params": {page: "search", itemID: null }
			},
			{
				"page": StaticContent,
				"icon": "bus",
				"title": "Transport<br>Links",
				"params": {page: "transport", itemID: [3438, 3476], poppage: false }
			},
			{
				"page": StaticContent,
				"icon": "phone-handset",
				"title": "Emergency Contacts",
				"params": {page: "emergency", itemID: [3440, 3481], poppage: false }
			},
			{
				"page": VisitorSupport,
				"icon": "faq",
				"title": "FAQ<br>",
				"params": {page: "faq", itemID: [2140,2829] }
			}
		];
		
	}
	ionViewDidEnter() {
		setTimeout(function() {

			Splashscreen.hide();	
		}, 500);
		this.showIntro = true;
		this.videoBg.nativeElement.play();
		this.ref.detectChanges();
	}
	ionViewDidLeave() {
		// this.videoBg.nativeElement.src = "";
		// this.videoBg.nativeElement.load();
	}
	goToPage(item) {
		this.nav.setRoot(item.page,{},{ animate: true, direction: "back"});
	}

	footerLink(page) {
		if(page.params.page!="search") {
			this.nav.setRoot(page.page, page.params, { animate: true, direction: "back"});
		} else {
			this.events.publish("search:open", true);
		}
	}

	changeLang() {
		this.language.setLanguage("en")
	}

	getRandom(min, max) {
	    return Math.floor(Math.random() * (max - min) + min);
	}
	// ionViewDidLeave(){
	// 	this.hw.stopWidgets();
	// }
	// ionViewDidEnter(){
	// 	this.hw.startWidgets();
	// }
}
