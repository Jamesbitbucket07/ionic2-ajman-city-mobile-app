import { Component, forwardRef, ViewChild  } from '@angular/core';
import { Content, ModalController, LoadingController } from 'ionic-angular';
import {FilterPopover} from '../shared/filter-popover/filter-popover.component';

import {
  DbService,
  HttpClient,
  AuthorizationService,
  ImageCache,
  AppSettings,
  TopBar,
  CardList
} from '../shared';


@Component({
  templateUrl: 'build/+trips/trips.component.html',
  providers: [
    DbService,
    HttpClient,
    AuthorizationService,
    ImageCache,
  ],
  directives: [TopBar, forwardRef(() => CardList)]
})

export class Trips {
  @ViewChild("content") content: Content;
  loading: any;
  didLoad: any;
  defaultFilterDuration:any;
  defaultFilterType:any;
  defaultFilterActivity:any;
  firstLoad: boolean = true;
  modalType: string;
  modal: any;
  filterValues:any;
  filterFields:any;
  scrollBar: boolean = false;
  constructor(public loadingCtrl: LoadingController, public dbService: DbService, public imageCache: ImageCache, private modalController : ModalController) {
    var that = this;
    this.filterFields = ['id','sortOrder'];
    this.filterValues = [
      { field:"numOfDays", value: '5 days', checked: false},
      { field:"numOfDays", value: '4 days', checked: false},
      { field:"numOfDays", value: '3 days', checked: false},
      { field:"numOfDays", value: '2 days', checked: false},
      { field:"numOfDays", value: '1 day', checked: false},
      { field:"interestTags", value: 'Active', checked: false},
      { field:"interestTags", value: 'Business', checked: false},
      { field:"interestTags", value: 'Family', checked: false},
      { field:"interestTags", value: 'Friends', checked: false},
      { field:"interestTags", value: 'Romantic', checked: false},
      { field:"interestTags", value: 'Solo', checked: false},
      { field:"activityTags", value: 'Beach', checked: false},
      { field:"activityTags", value: 'Culture', checked: false},
      { field:"activityTags", value: 'Cycling', checked: false},
      { field:"activityTags", value: 'Desert', checked: false},
      { field:"activityTags", value: 'Golf', checked: false},
      { field:"activityTags", value: 'Scenic', checked: false},
      { field:"activityTags", value: 'Shopping', checked: false},
      { field:"activityTags", value: 'Spa', checked: false},
      { field:"activityTags", value: 'Sports', checked: false},
      { field:"activityTags", value: 'Watersports', checked: false}
      
    ]; 

     
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",      
     duration: 2000,
      dismissOnPageChange: false
    });
    this.loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
    //this.presentLoading();

  }
  presentLoading() {
    this.loading.present();
  }
  dismissLoading() {
    //this.loading.dismiss();
  }
  scrolled(value) {
    this.scrollBar = value;
  }
   // presentModal(type) {

  //   var that = this;
  //   var filter;
  //     if(type == "itenerary_duration")
  //       filter = this.defaultFilterDuration;
  //     if(type == "itenerary_activity")
  //       filter = this.defaultFilterActivity;
  //     if(type == "itenerary_type")
  //       filter = this.defaultFilterType;

  //     this.modalType = type;
  //     var orStatement = [];
    
  //       this.modal = this.modalController.create(FilterPopover, {type: type, filter:filter});
  //       this.modal.present();
  //       this.modal.onDidDismiss(function(filter){

  //           that.firstLoad = false;
  //           that.presentLoading();
  //           console.log("filter after", filter);

  //           if(type == "itenerary_duration") {
  //             var valid = that.checkFilter(filter);
  //             console.log("duration", filter, valid);

  //             that.defaultFilterDuration = (valid)?filter:null;
  //           }
  //           if(type == "itenerary_activity"){
  //             var valid = that.checkFilter(filter);
  //             console.log("activity", filter, valid);

  //             that.defaultFilterActivity = (valid)?filter:null;
  //           }
  //           if(type == "itenerary_type"){
  //             var valid = that.checkFilter(filter);
  //             console.log("type", filter, valid);
  //             that.defaultFilterType = (valid)?filter:null;
  //           }


  //           if (that.defaultFilterDuration.oneDay)
  //             orStatement.push({ numOfDays: { $eq: "1 day" } });
  //           if (that.defaultFilterDuration.twoDays)
  //             orStatement.push({ numOfDays: { $eq: "2 days" } });
  //           if (that.defaultFilterDuration.threeDays)
  //             orStatement.push({ numOfDays: { $eq: "3 days" } });  
  //           if (that.defaultFilterDuration.fourDays)
  //             orStatement.push({ numOfDays: { $eq: "4 days" } });    
  //           if (that.defaultFilterDuration.fiveDays)
  //             orStatement.push({ numOfDays: { $eq: "5 days" } }); 

  //           console.log(orStatement);

  //           that.dbService.getFiltered('itenerary', orStatement, ["numOfDays"], 0, 30).susbcribe(res => {
  //             console.log(res);
  //             that.dismissLoading();
  //           })
            
  //       });

  // }
  // checkFilter(filter) {
  //   for(var i in filter) {
  //     var valid = false;
  //     if(filter[i]) {
  //       valid = true;
  //       break;
  //     }
  //   }
  //   return valid;
  // }


}
