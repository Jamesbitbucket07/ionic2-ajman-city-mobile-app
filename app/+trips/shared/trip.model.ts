import { ImageCache } from '../../shared/services/image-cache.service';

import {
  AppSettings,
  AuthorizationService,
  DbService,
  HttpClient
} from '../shared';

/**
 * Trip Class
 */
export class Trip {
    id: number = null;
    searchIndex:string = null; 
    _id: string = null
    name: string = null;
    children: string = null;
    properties: string = null;
    days: Array<any> = [];
    numOfDays: string = null;
    bannerImage: string = null;
    featuredImage: string = null;
    cardImage: string = null;
    description: string = null;
    interestTags: any = [];
    bestTimeOfYear: string = null;
    keyFacts: string = null;
    activityTags: any = [];
    url: string = null;
    subtitle: string = null;
    alias: string = null;
    favorite: boolean = false;
    sortOrder:number = null;
    constructor(item: any, public imageCache) {
        var that = this;
        if (item) {
            
            for (var p in item)
            {
                if (!that.hasOwnProperty(p) || !item[p]) continue;
                if(p === 'children' ){
                    for(var d in item[p]) {

                        var aStr = item[p][d]['properties']["$values"][2]['Value'].match(/\{localLink:([^"]*})/g);
                        var accommodationIds = [];
                        aStr.forEach(function(t) {
                            var strA = t.replace("{localLink:", "").replace("}", "");
                            accommodationIds.push(parseInt(strA));
                        })



                        var location = item[p][d]['properties']["$values"][6]['Value'].split(",");
                        var lat = location[0];
                        var lng = location[1];
                        var mapZoom = location[2]
                        var dayArr = {
                            "name": item[p][d]['name'],
                            "accommodation": accommodationIds,
                            "activities": item[p][d]['properties']["$values"][3]['Value'],
                            "description": item[p][d]['properties']["$values"][4]['Value'],
                            "location": item[p][d]['properties']["$values"][6]['Value'],
                            "bgImage": AppSettings.SERVER_URL+item[p][d]['properties']["$values"][8]['Value'],
                            "latitude": lat,
                            "longitude": lng,
                            "mapZoom": mapZoom
                        };
                        
                        that.days.push(dayArr);
                    }
                } else if (p === 'properties' ){
                    var pVal = item[p]["$values"];
                    for(var d in pVal) {
                        var key = pVal[d]['Alias'];
                        if (!that.hasOwnProperty(key)) continue;
                        var value = pVal[d]['Value'];
                        if(key=="interestTags" || key=="activityTags") {
                            value = pVal[d]['Value'].split(",");
                        }
                        if(key=="bannerImage" && value) {
                            var fileName = value.split('//').pop().split('/').pop();
                            that.cardImage = "media/itinerary/" +  fileName;
                            that.featuredImage = that.cardImage;
                            value =  that.cardImage;



                        }
                        that[key] = value;
                    }
                } else if (p === 'bannerImage' ){
                        
                    
                } else {
                    that[p] = item[p];
                }
                
            }
            that.numOfDays = that.days.length + " day"
            that.numOfDays += (that.days.length>1)?"s":"";

        }
    }
}
