import { Component, Input, forwardRef, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Events, Content,ToastController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
// import { FileService } from '../../shared/services/file-service.service.ts';
import { TranslateService } from 'ng2-translate/ng2-translate'; 
import { InAppBrowser } from 'ionic-native';
import {
  TTSService,
  Favorites,
  AjmanMap,
  TopBar,
  Attraction,
  ImageSlider,
  TripAdvisor,
  VirtualTour,
  Panorama,
  ShareButton
} from '../../shared';
declare var cordova;

@Component({
  templateUrl: 'build/+attractions/attraction-details/attraction-details.component.html',
  providers:[
    Favorites,
    TripAdvisor
  ],
  directives: [forwardRef(() => AjmanMap), forwardRef(() => TopBar), forwardRef(() => TTSService), forwardRef(() => ShareButton)],
  styles: [`
  `]
})
export class AttractionDetails {
  @ViewChild('detailScroll') detailScroll: Content;
  item: Attraction;
  didLoad:boolean;
  tripAdvisorContent:string = null;
  icon: string;
  exitPage: boolean = false;
  enterPage: boolean = true;
  scrollTweak: boolean = false;
  scrollBar: boolean = false;
  imgHeight: any = window['innerHeight'];
    detailsBG: boolean = false;
    virtualTour: boolean = false;
    panoramaView: boolean = false;
    isScrolled: boolean = false;
  constructor(public translateService:TranslateService,public toastCtrl: ToastController,public tripAdvisor: TripAdvisor, public events: Events, private modalController : ModalController, public loadingCtrl: LoadingController, public navCtrl: NavController, params: NavParams, public favorites: Favorites) {
    this.item = params.get("item");
    console.log("attraction", this.item);
    if(this.item.id==1362) {
      this.virtualTour = true;
    } else {
      this.panoramaView = true;
    }
    if(this.item.tripAdvisorId)
      {
        this.tripAdvisor.getTripAdvisorDetailsForOneItem(this.item.tripAdvisorId).subscribe(res => {
          that.tripAdvisorContent = res;
        });
      }

    //this.presentLoading();
    var that = this;
    var icons = {
      "most-popular": [1361, 1367, 1370, 1373, 1380],
      "ajman": [1325, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370,  1375, 1376, 3126, 3129, 3132, 3127, 3120, 3125, 3121, 3119, 3128, 3123, 3131, 3122, 3116, 3117, 3118, 3124, 3130],
      "masfout": [1378, 1379, 1381, 3134, 3135, 3136, 3137],
      "manama": [1383, 1385, 1386, 1382, 3139, 3140, 3141, 3142],
      "nature": [1360, 1361, 1374],
      "beach": [1372, 1373]
    };

    for(var c in icons) {
      var arr = icons[c];
      if(arr.indexOf(this.item.id) > -1) {
        this.icon = c;
      }
    }
    events.subscribe("page:exit", function() {
       that.exitPage = true;
    });

  }

  ngAfterViewInit() {
        this.detailScroll.addScrollListener((event) => {
             var topPos = event.target.querySelector(".details-top").scrollHeight+50;
             this.enterPage = false;
             // if(event.target.scrollTop>topPos) {
             //   console.log("scrollTweak", true);
             //   this.scrollTweak = true;
             // } else {
             //   console.log("scrollTweak", false);
             //   this.scrollTweak = false;
             // }
             if (event.target.scrollTop > 10) {
                if(!this.scrollBar) {
                  this.scrollBar = true;
                }
                
              } else {
                if(this.scrollBar) {
                  this.scrollBar = false;  
                }
                
              }
        });
    }

     ionViewDidEnter() {
      this.detailsBG = true
    }
  presentLoading() {
    let loading = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000,
      dismissOnPageChange: false
    });
    loading.present();
    loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
  }

  getItems() {
    return [this.item];
  }
 openURL(day) {
   cordova.InAppBrowser.open('https://www.google.com/maps/place/'+day.latitude+','+ day.longitude+'/@'+day.location+'z', '_system')
  }
  toggleFavorite() {
    var str = "";

    if (this.favorites.itemIsFavorite(this.item.id)) {
      str = "Item removed from favorites!";
      this.item.favorite = false;
      this.favorites.removeFromFavorites(this.item.id);
    }
    else {
      str = "Added to favourites!";
      this.item.favorite = true;
      this.favorites.addToFavorites(this.item.id);
    }

    this.translateService.getTranslation(localStorage.getItem("currentLang")).subscribe(res => {
      if (res[str])
        str = res[str];
      var toast = this.toastCtrl.create({
        message: str,
        duration: 3000,
        position: 'bottom'
      })
      toast.present();
    });
    this.events.publish("favourites:change", true);

  }
  showImageSlider(){
    let modal = this.modalController.create(ImageSlider,{images:this.item.images});
    console.log(modal)
    modal.present();
  }
  openVirtualTour(){
     let modal = this.modalController.create(VirtualTour);
      console.log(modal)
      modal.present();
  }
  openPanoramas(){
     let modal = this.modalController.create(Panorama,{images:this.item.panorama});
      console.log(modal)
      modal.present();
  }


}
