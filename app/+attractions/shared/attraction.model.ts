import { ImageCache } from '../../shared/services/image-cache.service';
import { AppSettings } from '../../shared/services/app-settings.service';

/**
 * Attraction Categories
 */
export enum AttractionCategory {
    Beach = 0,
    Urban = 1,
    Mountain = 2,
    Mangroves = 3,
    Nature_And_Wild_Life = 4,
    Spa = 5,
    Heritage = 6,
    Dining = 7,
    Art_And_Culture = 8,
    Walking_And_Hiking = 9,
    Mountain_Biking = 10,
    Water_Sports = 11,
    Golf = 12,
    Desert_Safari = 13,
    Sightseeing = 14
}
//"Beach: 331,Urban: 332,Mountain: 333,Mangroves: 334,Nature & Wild Life: 653,Spa: 654,Heritage: 655,Dining: 656,Arts & Culture: 657,Walking & Hiking: 658,Mountain Biking: 659,Water Sports: 660,Golf: 661,Desert Safari: 662,Sightseeing: 663"

/**
 * Attraction Class
 */
export class Attraction {
    id: number = null;
    searchIndex:string = null; 
    name: string = null;
    location: string = null;
    tripAdvisorId: string = null;
    mapContent: string = null;
    description: string = null;
    subtitle: string = null;
    keyInfo: boolean = null;
    images: Array<string> = null;
    panorama: Array<string> = null;
    featuredImage: string = null;
    bannerImage: string = null;
    cardImage: string = null;
    category: AttractionCategory = null;
    updateDate: Date = null;
    featured: Boolean = false;
    latitude: number = null;
    longitude: number = null;
    favorite: boolean = false;
    url: string = null;
    alias: string = null;
    panoramaImage: any = null;
    icon: string = './imgs/pins/pin-destination-hd.png';
    children: Array<number> = null;
    sortOrder:number = null;

//     public static getFilterStatement(ids:Array<number>,filterValues:any)
//    {
//        var orStatement = [];
        
//         if(filterValues.beach)
//             orStatement.push({category:{$eq:"331"}});
//         if(filterValues.urban)
//             orStatement.push({category:{$eq:"332"}});
//         if(filterValues.mountain)
//             orStatement.push({category:{$eq:"333"}});
//         if(filterValues.mangroves)
//             orStatement.push({category:{$eq:"334"}});
        

//         var filter = { $and:[{id: { $in: ids }},{$or:orStatement}] };

//         return filter;
//    }


    constructor(item: any, public imageCache) {
        if (item) {
            console.log("attraction", item)
            for (var field in item) {
                if (!this.hasOwnProperty(field) || !item[field]) continue;

                if(field == "children" && item[field]) 
                {
                    this.children = new Array<number>();
                    item[field].forEach(c => {
                        this.children.push(c.id);
                    })
                }
                else{
                    this[field] = item[field];
                }

                
            }
            var allValues = item["properties"]["$values"];
            for (var i = 0; i < allValues.length; i++) {
                if (!this.hasOwnProperty(allValues[i]["Alias"]) || !allValues[i]["Value"]) continue;
              
                if(allValues[i]["Alias"] === 'images'){
                    
                    this.images = allValues[i]["Value"].split(',').map(item => {
                        var fileName = item.split('//').pop().split('/').pop();
                        return "media/destination/" +  fileName;
                    });
                } else if(allValues[i]["Alias"] === 'panoramaImage') {
                    this.panorama = allValues[i]["Value"].split(',').map(item => {
                        var fileName = item.split('//').pop().split('/').pop();
                        return "media/panorama/" +  fileName;
                    });
                }
                else    
                    this[allValues[i]["Alias"]] = allValues[i]["Value"];
            }

            if (!this.featuredImage) {
                if(this.bannerImage){
                    var fileName = this.bannerImage.split('//').pop().split('/').pop();
                    this.cardImage = "media/destination/" +  fileName;
                    this.featuredImage = "media/destination/" +  fileName;
                }
            } else {
                var fileName = this.featuredImage.split('//').pop().split('/').pop();
                this.cardImage = "media/destination/" +  fileName;
                this.featuredImage = this.cardImage;
                
            }
                


            if (this.location) {
                var l = this.location.split(',');
                this.latitude = parseFloat(l[0]);
                this.longitude = parseFloat(l[1]);

            }
            
        }
    }
}
