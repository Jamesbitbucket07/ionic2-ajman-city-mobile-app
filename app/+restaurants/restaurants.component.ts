import { Component, forwardRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';

import {
  AuthorizationService,
  DbService,
  HttpClient,
  ImageCache,
  ContentService,
  CardList,
  TopBar,
  ConnectivityService
} from '../shared'

@Component({
  templateUrl: 'build/+restaurants/restaurants.component.html',
  providers: [
    DbService,
    AuthorizationService,
    HttpClient,
    ImageCache,
    ContentService,
    ConnectivityService
  ],
  directives: [forwardRef(() => TopBar), forwardRef(() => CardList)] 
})
export class Restaurants {
  didLoad: boolean;
  loading: any;
  filterValues:any;
  filterFields:Array<any>;
  scrollBar: boolean = false;
  constructor(public loadingCtrl: LoadingController) {
    this.filterValues = [
      { field:"type", value: 282, checked: false},
      { field:"type", value: 283, checked: false},
      { field:"type", value: 284, checked: false},
      { field:"type", value: 285, checked: false},
      { field:"type", value: 286, checked: false},
      { field:"type", value: 287, checked: false},
      { field:"type", value: 288, checked: false}
    ];


    this.filterFields = ['type','sortOrder'];


    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: false
    });
    this.loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
    //this.presentLoading();
  }
   presentLoading() {
    this.loading.present();
  }
  dismissLoading() {
    //this.loading.dismiss();
  }

  scrolled(value) {
    this.scrollBar = value;
  }
}
