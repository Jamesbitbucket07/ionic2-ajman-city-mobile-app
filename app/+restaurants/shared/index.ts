export { Restaurant } from './restaurant.model';
export { RestaurantDetails } from '../restaurant-details/restaurant-details.component';
export { Restaurants } from '../restaurants.component';
