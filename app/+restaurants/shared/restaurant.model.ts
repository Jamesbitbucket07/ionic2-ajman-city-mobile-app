import { AppSettings } from '../../shared/services/app-settings.service';
import { ImageCache } from '../../shared/services/image-cache.service';

export enum RestaurantCuisine {
    Afghani =             231,
    African =             232,
    American =            233,
    Arabian =             234,
    Asian =               235,
    Bangladeshi =         236,
    Belgian =             237,
    Bengali =             238,
    British =             239,
    Burger =              240,
    Chinese =             241,
    Continental =         242,
    Egyptian =            243,
    Emirati =             244,
    Ethiopian =           245,
    European =            246,
    Filipino =            247,
    French =              248,
    Fusion =              249,
    German =              250,
    Goan =                251,
    Gujarati =            252,
    Hyderabadi =          253,
    Indian =              254,
    Indonesian =          255,
    International =       256,
    Iranian =             257,
    Italian =             258,
    Japanese =            259,
    Kerala =              260,
    "Latin American" =    261,
    Lebanese =            262,
    Malaysian =           263,
    Mediterranean =       264,
    Mexican =             265,
    "Middle Eastern" =    266,
    Moroccan =            267,
    Nepalese =            268,
    "North Indian" =      269,
    Pakistani =           270,
    Persian =             271,
    Portuguese =          272,
    Russian =             273,
    "South Indian" =      274,
    Spanish =             275,
    "Sri Lankan" =        276,
    Syrian =              277,
    "Tex-Mex" =           278,
    Thai =                279,
    Turkish =             280,
    Vegetarian =          281
}

export enum RestaurantType {
    Fast_Food,
    Casual_Dining,
    Fine_Dining,
    Café,
    Coffeehouse,
    Bakery,
    Pizzeria
}

/**
 * Restaurant Class
 */
export class Restaurant {
   id: number = null;
    searchIndex:string = null; 
    _id: string = null
    name: string = null;
    location: string = null;
    email: string = null;
    website: string = null;
    phone: string = null;
    streetAddress: string = null;
    description: string = null;
    subtitle: string = null;
    featuredImage: string = null;
    bannerImage: string = null;
    updateDate: Date = null;
    images: Array<string> = null;
    logo: string = null;
    mapContent: string = null;
    keyInfo: string = null;
    destinations: Array<number> = null;
    tripAdvisorId: string = null;
    featured: Boolean = false;
    cuisine: Array<RestaurantCuisine> = null;
    mainCuisine: string = null;
    type: RestaurantType = null;
    url: string = null;
    avragePrice: string = null;
    latitude: number = null;
    longitude: number = null;
    favorite: boolean = false;
    alias: string = null;
    icon: string = './imgs/pins/pin-dinner-hd.png';
    cardImage: string = null;
    sortOrder:number = null;
    // public static getFilterStatement(filterValues = any) {
    //     var orStatement = [];
    //     //"Fast food = 282,Casual dining = 283,Fine dining = 284,Café = 285,Coffeehouse = 286,Bakery = 287,Pizzeria = 288"
    //     if (filterValues.fast_food)
    //         orStatement.push({ type = { $eq = "282" } });
    //     if (filterValues.casual_dining)
    //         orStatement.push({ type = { $eq = "283" } });
    //     if (filterValues.fine_dining)
    //         orStatement.push({ type = { $eq = "284" } });
    //     if (filterValues.cafe)
    //         orStatement.push({ type = { $eq = "285" } });
    //     if (filterValues.coffeehouse)
    //         orStatement.push({ type = { $eq = "286" } });
    //     if (filterValues.Bakery)
    //         orStatement.push({ type = { $eq = "287" } });
    //     if (filterValues.pizzeria)
    //         orStatement.push({ type = { $eq = "288" } });


    //     var filter = { $and = [{ alias = { $eq = 'restaurant' } }, { $or = orStatement }] };


    //     return filter;
    // }

     public getInfo(id, type){
        var obj = {
           "avragePrice" : {
              289: "10-50",
              290: "50-100",
              291: "150-200",
              292: "200-300",
              293: "400+"
             },
             "cuisine": {
               231: "Afghani",
               232: "African",
               233: "American",
               234: "Arabian",
               235: "Asian",
               236: "Bangladeshi",
               237: "Belgian",
               238: "Bengali",
               239: "British",
               240: "Burger",
               241: "Chinese",
               242: "Continental",
               243: "Egyptian",
               244: "Emirati",
               245: "Ethiopian",
               246: "European",
               247: "Filipino",
               248: "French",
               249: "Fusion",
               250: "German",
               251: "Goan",
               252: "Gujarati",
               253: "Hyderabadi",
               254: "Indian",
               255: "Indonesian",
               256: "International",
               257: "Iranian",
               258: "Italian",
               259: "Japanese",
               260: "Kerala",
               261: "Latin American",
               262: "Lebanese",
               263: "Malaysian",
               264: "Mediterranean",
               265: "Mexican",
               266: "Middle Eastern",
               267: "Moroccan",
               268: "Nepalese",
               269: "North Indian",
               270: "Pakistani",
               271: "Persian",
               272: "Portuguese",
               273: "Russian",
               274: "South Indian",
               275: "Spanish",
               276: "Sri Lankan",
               277: "Syrian",
               278: "Tex-Mex",
               279: "Thai",
               280: "Turkish",
               281: "Vegetarian",
             },
             "type": {
                 282: "Fast food",
                 283: "Casual dining",
                 284: "Fine dining",
                 285: "Café",
                 286: "Coffeehouse",
                 287: "Bakery",
                 288: "Pizzeria",
             }
           }
         return obj[type][id];
       }

    constructor(item: any, public imageCache) {
        if (item) {
            for (var field in item) {
                if (!this.hasOwnProperty(field) || !item[field]) continue;


                this[field] = item[field];
            }
            var allValues = item["properties"]["$values"];
            for (var i = 0; i < allValues.length; i++) {
                if (!this.hasOwnProperty(allValues[i]["Alias"]) || !allValues[i]["Value"]) continue;

                this[allValues[i]["Alias"]] = allValues[i]["Value"];

                if(allValues[i]["Alias"] === 'avragePrice'){
                   this[allValues[i]["Alias"]] = this.getInfo(allValues[i]["Value"], allValues[i]["Alias"]);
                 }
                 if(allValues[i]["Alias"] === 'cuisine'){
                     var c = allValues[i]["Value"].split(",");
                     var cuisineArr = [];
                     for(let i in c) {
                         var info = this.getInfo(c[i], "cuisine");
                         cuisineArr.push(info);
                     }
                     this['mainCuisine'] = cuisineArr[0];
                     this[allValues[i]["Alias"]] = cuisineArr.join(",");
                 }
                if(allValues[i]["Alias"] === 'images'){
                    
                    this.images = allValues[i]["Value"].split(',').map(item => {
                        var fileName = item.split('//').pop().split('/').pop();
                        return "media/restaurant/" +  fileName;
                    });
                }
                
            }

            if (!this.featuredImage) {
                if(this.bannerImage){
                    var fileName = this.bannerImage.split('//').pop().split('/').pop();
                    this.cardImage = "media/restaurant/" +  fileName;
                }
            } else {
                var fileName = this.featuredImage.split('//').pop().split('/').pop();
                this.cardImage = "media/restaurant/" +  fileName;
                this.featuredImage = this.cardImage;
                
            }

            if (this.location) {
                var l = this.location.split(',');
                this.latitude = parseFloat(l[0]);
                this.longitude = parseFloat(l[1]);

            }

        }
    }
}
