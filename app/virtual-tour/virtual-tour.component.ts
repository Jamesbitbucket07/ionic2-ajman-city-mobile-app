import { Component, OnInit } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { TranslatePipe, TranslateService, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { Http, Headers } from '@angular/http';

import { 
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar
} from '../shared'

declare var pannellum;

@Component({
	templateUrl: 'build/virtual-tour/virtual-tour.component.html',
	 directives: [TopBar],
	 providers: [TranslateService, TranslateLoader, LanguageService, 
    { 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    }],
    pipes: [TranslatePipe]
})
export class VirtualTour implements OnInit {
	isLoaded: boolean = false;
	constructor(public viewCtrl:ViewController) {}

	ngOnInit() {
		pannellum.viewer('panorama', {
			"loadingText": "Please wait...",
			"showFullscreenCtrl": false,
			"autoLoad": true,
			"showZoomCtrl": false,
		    "default": {
		        "firstScene": "01",
		        "sceneFadeDuration": 1000
		    },

		    "scenes": {
		        "01": {
		        	
		            "type": "equirectangular",
		            "panorama": "imgs/vtour/01.jpg",
		            "hotSpots": [
		                {
		                    "hfov": 100,
				            "pitch": 0,
				            "yaw": 0,
		                    "type": "scene",
		                    "sceneId": "02"
		                }
		            ]
		        },

		        "02": {
		 
		            "type": "equirectangular",
		            "panorama": "./imgs/vtour/02.jpg",
		            "hotSpots": [
		                {
		                    "pitch": -3,
		                    "yaw": 179,
		                    "hov": 100,
		                    "type": "scene",
		                    "sceneId": "01",
		                },
		                {
		                    "hfov": 100,
				            "pitch": 0,
				            "yaw": 0,
		                    "type": "scene",
		                    "sceneId": "03"
		                }
		            ]
		        },

		        "03": {
		 
		            "type": "equirectangular",
		            "panorama": "./imgs/vtour/04.jpg",
		            "hotSpots": [
		                {
		                    "pitch": -2.44,
		                    "yaw": -181.23,
		                    "hov": 100,
		                    "type": "scene",
		                    "sceneId": "02",
		                    "targetYaw": -171,
		                    "targetPitch": -8.75
		                },
		                {
		                    "hfov": 100,
				            "pitch": 0,
				            "yaw": 0,
		                    "type": "scene",
		                    "sceneId": "04"
		                }
		            ]
		        },

		        "04": {
		 
		            "type": "equirectangular",
		            "panorama": "./imgs/vtour/06.jpg",
		            "hotSpots": [
		                {
		                    "pitch": -2.44,
		                    "yaw": -181.23,
		                    "hov": 100,
		                    "type": "scene",
		                    "sceneId": "02",
		                    "targetYaw": -171,
		                    "targetPitch": -8.75
		                },
		                {
		                    "hfov": 100,
				            "pitch": 0,
				            "yaw": 0,
		                    "type": "scene",
		                    "sceneId": "04"
		                }
		            ]
		        }
		    }
		});
		this.isLoaded = true;
	}

	closeModal() {
		this.viewCtrl.dismiss();
	}
}