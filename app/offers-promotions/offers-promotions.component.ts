import { Component, OnInit, forwardRef, ChangeDetectorRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/forkJoin';
import {
	ContentService,
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar,
  	CardList,
  	DbService,
    HttpClient,
    AuthorizationService,
    ImageCache
} from '../shared'


@Component({
	templateUrl: 'build/offers-promotions/offers-promotions.component.html',
	providers: [
    ContentService,
    DbService,
    HttpClient,
    AuthorizationService,
    ImageCache,
  ],
  directives: [TopBar, forwardRef(() => CardList)]
})
export class OffersPromotions implements OnInit {
	page: any;
	items: any = null;
	loading: any;
	didLoad: any;
	scrollBar: boolean = false;
	constructor(public ref: ChangeDetectorRef, public loadingCtrl: LoadingController, public dbService: DbService) {
		var that = this;
		this.loading = this.loadingCtrl.create({
	      content: "Please wait...",      
	      duration: 2000,
	      dismissOnPageChange: false
	    });
	    this.loading.onDidDismiss(()=>{
	      this.didLoad = true;
	    });
	    //this.presentLoading();
	    
	}

	ngOnInit() {
		var that = this;

		// this.items = [1351, 1427, 1428, 1429, 1430, 2146, 2823, 2824, 2826, 2825, 2827, 2831]
		this.dbService.getFiltered({id: {$in : [3484]}}, [], ["id"], 0, 30).subscribe(res => {
			that.page = res.items[0]
			console.log(res);
	    	var items = res.items[0]['childrenPages'];
	    	that.items = items;
	    	that.ref.detectChanges();
	    	// observer.next(items)
	    })

	}

	// getItems() {
	// 	var that = this
	// 	return Observable.create(observer => {
	// 		this.dbService.getFiltered({id: {$in : [1351, 2823]}}, [], ["id"], 0, 30).subscribe(res => {
	// 			console.log("aboutpage", res.items[0]['childrenPages'])
	// 	    	var items = res.items[0]['childrenPages'];
	// 	    	that.items = items;
	// 	    	observer.next(items)
	// 	    })
	// 	})
	// }
	presentLoading() {
      this.loading.present();
    }
    dismissLoading() {
      //this.loading.dismiss();
    }
    scrolled(value) {
    this.scrollBar = value;
  }
}