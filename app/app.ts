import { Component, PLATFORM_PIPES, enableProdMode } from '@angular/core';
enableProdMode();
import { ionicBootstrap, Platform } from 'ionic-angular';
import { TranslateService, TranslateLoader, TranslateStaticLoader, TranslatePipe } from 'ng2-translate/ng2-translate';
import { HTTP_PROVIDERS, Http } from '@angular/http';
import { StatusBar, Keyboard, Geolocation } from 'ionic-native';
import { LanguageService } from "./shared/services/language.service"
// import { DbService } from "./shared/services/db.service"
import { AjmanNav } from './shared/nav/nav.component';
import { FileService } from "./shared/";
import {
    GOOGLE_MAPS_PROVIDERS
} from 'angular2-google-maps/core';

import { ConnectivityService } from './shared';

declare var ImgCache: any;
declare var cordova: any;
declare var CameraPreview: any;



@Component({
  templateUrl: 'build/app.html',
  directives: [AjmanNav],
  pipes: [TranslatePipe],
  providers: [
  { 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    },
    TranslateService,
    LanguageService
   ]
})

class AjmanTravelApp {

  notLoaded: boolean = true;
   constructor(private platform: Platform, private language: LanguageService){

    this.initializeApp();
    var currLang = (window['localStorage']['currentLang'])?window['localStorage']['currentLang']:"en";
    language.setLanguage(currLang);
    window['localStorage']['excluded_ids'] = "0";
    if(window['localStorage']['tourOnStart'] == "true" || !window['localStorage']['db_installed_fix']) {
      window['localStorage']['db_installed_fix'] = true;
      window['localStorage']['introLoaded'] = true;
      if(!window['localStorage']['db_installed']) {
          window['localStorage']['langSel'] = false;
      } else {
        window['localStorage']['langSel'] = true;
      }

    } else {
      window['localStorage']['tourOnStart'] = false;
      window['localStorage']['langSel'] = true;
    }

  }

  initializeApp() {
    var that = this;
    this.platform.ready().then(() => {
      console.log(window);
      // console.log("cloud", CloudStorage);
      StatusBar.hide();
      Keyboard.hideKeyboardAccessoryBar(true);
      Keyboard.disableScroll(true);
      let rect = {
        x: 0, 
        y: 70, 
        width: this.platform.width(), 
        height: this.platform.width(),
        camera: 'back',
        tapPhoto: false,
        previewDrag: false,
        toBack: true,
        alpha: 0
      };
     
      cordova.plugins.diagnostic.requestMicrophoneAuthorization(function(success) {
          console.log("Microphone is " + success);

        },
        function(err) {
          console.log("Microphone is " + err);
        }
      );

      cordova.plugins.diagnostic.requestLocationAuthorization(function(success) {
          console.log("Location is " + success);
        },
        function(err) {
          console.log("Location is " + err);
        }
      )

      cordova.plugins.diagnostic.requestCameraAuthorization(function(success) {
          console.log("Camera is " + success);
          if(that.platform.is("ios")) {
            CameraPreview.startCamera(rect, function() {
              console.log("CameraOn")
              CameraPreview.stopCamera()
            }, function(e) {
              console.log(e);
            })  
          }
          
        },
        function(err) {
          console.log("Camera is " + err);
        }
      )
      window.addEventListener("orientationchange", function() {
        console.log(window.orientation);
      }, false);
      

      // ImgCache.init(() => {
      //   console.log('ImgCache init: success!');

      //   // from within this function you're now able to call other ImgCache methods
      //   // or you can wait for the ImgCacheReady event

      // }, () => {
      //   console.log('ImgCache init: error! Check the log for errors');
      // });
    });
  }

}
ionicBootstrap(AjmanTravelApp, [
    {   
      provide: PLATFORM_PIPES,
      useValue: TranslatePipe,
      multi: true
    },
    { 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    },
    ConnectivityService, 
    GOOGLE_MAPS_PROVIDERS
  ]
);


