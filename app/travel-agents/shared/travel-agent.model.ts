import { ImageCache } from '../../shared/services/image-cache.service';
import { AppSettings } from '../../shared/services/app-settings.service';

/**
 * Travel Agents Class
 */
export class TravelAgent {
    id: number = null;
    searchIndex:string = null; 
    _id: string = null;
    name: string = null;
    description: string = null;
    subtitle: string = null;
    featuredImage: string = null;
    bannerImage: string = null;
    cardImage: string =  null;
    updateDate: Date = null;
    eventStartDate: Date = null;
    eventEndDate: Date = null;
    eventAddress: string = null;
    location: string = null;
    latitude: number = null;
    longitude: number = null;
    favorite: boolean = false;
    alias: string = null;
    icon: string = './imgs/pins/pin-todo-hd.png';
    sortOrder:number = null;
    webiste: string = null;
    email: string = null;
    phone: string = null;
    constructor(item: any, public imageCache) {
        if (item) {
            for (var field in item) {
                if (!this.hasOwnProperty(field) || !item[field]) continue;


                this[field] = item[field];
            }
            var allValues = item["properties"]["$values"];
            
            for (var i = 0; i < allValues.length; i++) {
                if (!this.hasOwnProperty(allValues[i]["Alias"]) || !allValues[i]["Value"]) continue;

                this[allValues[i]["Alias"]] = allValues[i]["Value"];
            }

           if (!this.featuredImage) {
                if(this.bannerImage){
                    var fileName = this.bannerImage.split('//').pop().split('/').pop();
                    this.cardImage = "media/travelAgent/" +  fileName;
                }
            } else {
                var fileName = this.featuredImage.split('//').pop().split('/').pop();
                this.cardImage = "media/travelAgent/" +  fileName;
                this.featuredImage = this.cardImage;
                
            }

            if (this.location) {
                var l = this.location.split(',');
                this.latitude = parseFloat(l[0]);
                this.longitude = parseFloat(l[1]);

            }

        }

    }
}
