import { Component, forwardRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';

import {
  AuthorizationService,
  DbService,
  HttpClient,
  ImageCache,
  ContentService,
  CardList,
  TopBar,
  ConnectivityService
} from '../shared'

@Component({
  templateUrl: 'build/+events/events.component.html',
  providers: [
    DbService,
    AuthorizationService,
    HttpClient,
    ImageCache,
    ContentService,
    ConnectivityService
  ],
  directives: [forwardRef(() =>TopBar), forwardRef(() => CardList)] 
})
export class Events {
  didLoad: boolean;
  loading: any;
  filterFields = ["id","sortOrder"];
  scrollBar: boolean = false;
  constructor(public loadingCtrl: LoadingController) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: false
    });
    this.loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
    //this.presentLoading();
  }
   presentLoading() {
    this.loading.present();
  }
  dismissLoading() {
    //this.loading.dismiss();
  }
  scrolled(value) {
    this.scrollBar = value;
  }

}
