import { Component, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Events as EventManager, ViewController, Nav, NavParams } from 'ionic-angular';
import { InAppBrowser } from 'ionic-native';
import { Http, Headers } from '@angular/http';
import { TranslatePipe, TranslateService, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { LanguageService } from "../shared/services/language.service";
import {Favorites} from "../shared/";
import 'rxjs/add/operator/map';

import { 
  ToggleNav,
  TopBar
} from '../shared'

declare var cordova;
declare var fx;

let numeral = require('numeral');

@Component({
  templateUrl: 'build/currency-converter/currency-converter.component.html',
  providers: [Favorites, TranslateService, TranslateLoader, LanguageService,Nav, 
    { 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    }],
    pipes: [TranslatePipe],
    directives: [TopBar]
  // host: {
  // 	"class": "settings"
  // }
})

export class CurrencyConverter {
  item: any;
  numberValue: any = "0";
  formatedNumber: any;
  converted: any = "0.00";
  showNumber: any = "0.00";
  source: any;
  currencies: any;
  currencyFrom: string = "AED";
  currencyTo: string = "USD";
  selectModel: any;
  fromTitle: string = "UAE Dirham";
  isAEDFrom: boolean = true;
  convertedNumber: any = 0;
  selectOptions: any;
  detailsBG: boolean = false;
  scrollBar: boolean = false;
  constructor(event: EventManager, public ref: ChangeDetectorRef, public http: Http, public params: NavParams) {
    this.item = this.params.get('item');
  	this.getRates().subscribe(
      data => {
        fx.rates = data.rates
        console.log(data);
      },
      err => {
        console.log("err", err);
      });
    this.getCurrencies().subscribe(
      data => {
        this.source = data;

        var currArr = []
        for(var i in data){

          currArr.push({"code": i, "name": data[i]});
        }
        this.currencies = currArr;
    });
    this.selectModel = this.currencyTo;
    this.selectOptions = {
      title: 'Pizza Toppings',
      subTitle: 'Select your toppings'
    };

  }
  ionViewDidEnter() {
    this.detailsBG = true;
    this.refreshNumber();
    
  }
  getRates() {
    return this.http.get("http://openexchangerates.org/api/latest.json?app_id=35f6fda4f25a4f42a0e957b05f6b65bb").map(res => res.json());
  }
  getCurrencies() {
    return this.http.get("json/currencies.json").map(res => res.json());
  }
   clickNumber(e) {
      this.numberValue = this.numberValue+""+e.target.innerText;
      this.refreshNumber()
       console.log("after refresh numberclick" , e.target.innerText + this.numberValue);
   }
   removeNumber() {
     this.numberValue = this.numberValue.substring(0, this.numberValue.length-1);
     this.numberValue = (this.numberValue=="")?"0":this.numberValue;
     this.refreshNumber()
   } 
   refreshNumber() {
    var num = Math.round(parseInt(this.numberValue))
    this.formatedNumber = num/100;
    this.convertedNumber = fx(this.formatedNumber).from(this.currencyFrom).to(this.currencyTo);
    if(this.isAEDFrom) {
      this.converted = this.formatMoney(this.convertedNumber);
      this.showNumber = this.formatMoney(this.formatedNumber);  
    } else {
      this.converted = this.formatMoney(this.formatedNumber);
      this.showNumber = this.formatMoney(this.convertedNumber);
    }
    

   }

   formatMoney(num){
      return numeral(num).format('0,0.00');
   }
   selectCurrency(e){
     if(this.isAEDFrom) {
       this.currencyTo = e;
       this.currencyFrom = "AED";

     } else {
       this.currencyFrom = e;  
       this.currencyTo = "AED";
     }
     
     this.selectModel = e;
     this.refreshNumber();
   }

   switchCurrency() {
     var oldFrom = this.currencyFrom
     var oldTo = this.currencyTo;
     this.currencyFrom = oldTo;
     this.currencyTo = oldFrom;
     // this.fromTitle = (oldFrom=="AED")?this.source[oldTo]:"UAE Dirham";
     this.isAEDFrom = !this.isAEDFrom;
     this.numberValue = ""+(this.convertedNumber.toFixed(2)*100);
     
     this.refreshNumber();
     // alert("after refresh switch" + this.numberValue);
   }

}