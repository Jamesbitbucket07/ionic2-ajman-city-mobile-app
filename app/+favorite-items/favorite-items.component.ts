import { Component, forwardRef, ViewChild, ChangeDetectorRef  } from '@angular/core';
import { Content, ModalController, Events, LoadingController } from 'ionic-angular';
import {FilterPopover} from '../shared/filter-popover/filter-popover.component';

import {
  ContentService,
  DbService,
  HttpClient,
  AuthorizationService,
  ImageCache,
  AppSettings,
  TopBar,
  CardList,
  Favorites
} from '../shared';


@Component({
  templateUrl: 'build/+favorite-items/favorite-items.component.html',
  providers: [
    ContentService,
    DbService,
    HttpClient,
    AuthorizationService,
    Favorites,
    ImageCache,
  ],
  directives: [forwardRef(() => TopBar), forwardRef(() => CardList)]
})

export class FavoriteItems {
  @ViewChild('cardList') mapCmp: CardList;
  allFavorites: Array<any> = null;
  loading: any;
  filterFields:any;
  filterValues:any;
  scrollBar: boolean = false;
  isNearby: boolean = true;
  constructor(public loadingCtrl: LoadingController, public events: Events, public ref: ChangeDetectorRef, public favorites: Favorites, public contentService: ContentService, public imageCache: ImageCache, private modalController : ModalController) {
    var that = this;
    this.filterValues = [
      { field:"alias", value: 'hotel', checked: false},
      { field:"alias", value: 'restaurant', checked: false},
      { field:"alias", value: 'activity', checked: false},
      { field:"alias", value: 'destination', checked: false},
      { field:"alias", value: 'event', checked: false}
    ];


    this.filterFields = ["id","sortOrder"];

    events.subscribe("favourites:change", function(e) {
      that.getFavorites();
    })
 
    // this.loading.present();
  }

  ngAfterViewInit() {
    this.getFavorites();
  }
  dismissLoading(e) {
    //this.loading.dismiss();
  }

  scrolled(value) {
    this.scrollBar = value;
  }

  // public static getFilterStatement(ids:Array<number>, filterValues:any)
  //  {
  //      var orStatement = [];
        
        

  //       if(filterValues.hotel)
  //           orStatement.push({alias:{$eq:"hotel"}});
  //       if(filterValues.restaurant)
  //           orStatement.push({alias:{$eq:"restaurant"}});
  //       if(filterValues.activity)
  //           orStatement.push({alias:{$eq:"activity"}});
  //       if(filterValues.destination)
  //           orStatement.push({alias:{$eq:"destination"}});
  //       if(filterValues.event)
  //           orStatement.push({alias:{$eq:"event"}});       

  //       var filter = { $and:[{id: { $in: ids }},{$or:orStatement}] };

  //       return filter;
  //  }
  

  getFavorites(){

    this.allFavorites = null;
    var favs = this.favorites.getAllFavorites();
    this.allFavorites = favs;
    // console.log("favs",favs)
    // console.log(favs);
    // this.contentService.getByIds(favs).subscribe(res => {
    //   console.log("res all favs",res)
    //   this.allFavorites = res;
    // });

  }
  
}
