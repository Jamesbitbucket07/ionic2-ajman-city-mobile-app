import { Component, OnInit, forwardRef, ViewChild  } from '@angular/core';
import { NavParams, LoadingController, Events, Content,ToastController } from 'ionic-angular';
// import { FileService } from '../shared/services/file-service.service.ts';

import {
	DbService,
  ContentService,
  TopBar,
  CardList,
  TTSService,
  Favorites,
  ShareButton
} from '../shared';

@Component({
	providers: [ContentService, Favorites],
	directives: [TopBar, forwardRef(() => TTSService), forwardRef(() => ShareButton)],
	templateUrl: 'build/+content/content.component.html',
})
export class StaticContent  {

	item: any = null;
	exitPage: boolean = false;
	enterPage: boolean = true;
	scrollTweak: boolean = false;
	scrollBar: boolean = false;
  imgHeight: any = window['innerHeight'];
  detailsBG: boolean = false;
  popButton: boolean = true;
	@ViewChild('detailScroll') detailScroll: Content;
	constructor(public toastCtrl: ToastController, public events: Events, private dbService: DbService, public params: NavParams, public favorites: Favorites) {
		var that = this;
		if(params.get("itemID")) {
      this.popButton = params.get("poppage")
	        that.dbService.getFiltered({id: {$in: params.get("itemID")}},null, ["id"], 0, 100)
			    .subscribe(res => {
                	  that.item = res.items[0];
                    if(!that.item.bannerImage) {
                      that.item.bannerImage = that.item.featuredImage; 
                    }
                }); 
			
		} else {
			this.item = params.get("item");	
      // this.fileService.getFile(this.item.bannerImage, "static")
		}

    console.log(this.item)

		
		events.subscribe("page:exit", function() {
         that.exitPage = true;
      });
	}

	 ionViewDidEnter() {
        this.detailScroll.addScrollListener((event) => {
             var topPos = event.target.querySelector(".static-details-top").scrollHeight+50;
             this.enterPage = false;
             // if(event.target.scrollTop>topPos) {
             //   console.log("scrollTweak", true);
             //   this.scrollTweak = true;
             // } else {
             //   console.log("scrollTweak", false);
             //   this.scrollTweak = false;
             // }
             console.log("scroll")
             if (event.target.scrollTop > 10) {
                if(!this.scrollBar) {
                  this.scrollBar = true;
                }
                
              } else {
                if(this.scrollBar) {
                  this.scrollBar = false;  
                }
                
              }
        });
      this.detailsBG = true
    }
	 toggleFavorite() {
      if (this.favorites.itemIsFavorite(this.item.id)) {
          this.item.favorite = false;
          this.favorites.removeFromFavorites(this.item.id);
      }
      else {
          this.item.favorite = true;
          this.favorites.addToFavorites(this.item.id);
          var toast = this.toastCtrl.create({
            message: 'Added to favourites!',
            duration: 3000,
            position: 'bottom'
          })
          toast.present();
      }
      this.events.publish("favourites:change", true);

  }

}