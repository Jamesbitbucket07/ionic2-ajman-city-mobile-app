import { ImageCache } from '../../shared/services/image-cache.service';

import {
  AppSettings,
  AuthorizationService,
  DbService,
  HttpClient
} from '../../shared';

/**
 * TextPage Class
 */
export class TextPage {
    id: number = null;
    searchIndex:string = null; 
    _id: string = null
    name: string = null;
    children: string = null;
    properties: string = null;
    bannerImage: string = null;
    description: string = null;
    subtitle: string = null;
    alias: string = null;
    url: string = null;
    content: any = null;
    featuredImage: string = null;
    cardImage: string = null;
    favorite: boolean;
    childrenPages: any = null;
    sortOrder:number = null;
    constructor(item: any, public imageCache) {
        var that = this;
        if (item) {
            if(item.id=="3433") {
                window['localStorage']["excluded_ids"] = item['properties']['$values'][13]['Value']
                
                return
            } else {
                for (var p in item)
                {
                    if (!that.hasOwnProperty(p) || !item[p]) continue;
                    if (p === 'properties' ){
                        var pVal = item[p]["$values"];
                        for(var d in pVal) {
                            var key = pVal[d]['Alias'];
                            if (!that.hasOwnProperty(key)) continue;
                            var value = pVal[d]['Value'];
                            
                            if(key=="bannerImage" && value) {
                                 var fileName = value.split('//').pop().split('/').pop();
                                 that['cardImage'] = "media/TextPage/" +  fileName;
                                 that['featuredImage'] = that['cardImage'];
                                 value = that['cardImage'];
                                //  value =  AppSettings.SERVER_URL + value + "?anchor=center&mode=crop&width=428&height=436";
                                // that['cardImage'] = value;
                                
                            }
                            if(key=="childrenPages" && typeof value === "string") {
                                var idArr = [];
                                value = value.split(",");
                                value.forEach(function(e) {
                                    idArr.push(parseInt(e))
                                })
                                value = idArr;
                            }
                            if(key=="content") {
                               
                                //ARABIC RGEX
                                var cValue = value.replace(/=(\'|\"|\\\")([ -0-9a-zA-Z:_@/{}]*[ -0-9a-zA-Z;_@/{}]*[ \u0600-\u065F\u066A-\u06EF\u06FA-\u06FF-0-9a-zA-Z:_@/{}]+[ \u0600-\u065F\u066A-\u06EF\u06FA-\u06FF-0-9a-zA-Z-_\.;_@/{}]*)*(\'|\"|\\\")/ig, "='$2'");
                                cValue = cValue.replace(/"\\'/g, '\"');
                                cValue = cValue.replace(/'\/media\/([-0-9a-zA-Z;_@/{}\.]*)*'/ig, "'"+AppSettings.SERVER_URL + "/media/$1'");
                                
                                try {
                                    var content =  JSON.parse(cValue);
                                } catch(err) {
                                       console.log("CatchJSON", item.id ,err, cValue);
                                       var content = null;
                                }

                                if(item.alias != "TextPage" && item.alias != "travelAgents") continue;
                                
                                try {
                                    content = content.sections[0].rows;
                                } catch(err) {
                                    console.log({"name": item.name, 'content': content});
                                    continue;
                                }

                                var contentObj = [];
                                var str = "";
                                for(var cX in content) {
                                    var c = content[cX];
                                    for( var aX in c['areas']) {
                                        var a = c['areas'][aX];
                                        for (var tX in a['controls']) {
                                            var ct = a['controls'][tX];
                                            if(typeof ct.value === "string") {
                                                str += ct.value;    
                                            }
                                            
                                        }
                                    }
                                }
                                
                                value = str

                            }
                            that[key] = value;
                        }
                    } else {


                        if(p=="name" && item[p]=="Help") {
                            that["name"] = "General FAQ";
                        } else if(p=="name" && item[p]=="Our People") {
                            that["name"] = "Sincerely Emirati";
                        } else if(p=="name" && item[p]=="About Ajman") {
                            that["name"] = "The Emirate of Ajman";
                        }  else {
                            that[p] = item[p];        
                        }
                        
                    }
                }

               if(item.id==2829) {
                    that["name"] = "معلومات عامة";
                } else if(item.id==2824) {
                    that["name"] = "أصالة إماراتية";
                } else if(item.id==2823) {
                    that["name"] = "إمارة عجمان";
                }

                if(item.id==1256) {
                    that['alias'] = 'travelAgents';
                    that['bannerImage'] = that['bannerImage'].replace("TextPage", "travelAgents");
                    that['cardImage'] = that['cardImage'].replace("TextPage", "travelAgents");
                    that['featuredImage'] = that['featuredImage'].replace("TextPage", "travelAgents");
                } else {
                    that['alias'] = 'static';    
                }
                
                that['description'] = item['subtitle'];
            }
            
        }

    }

    replaceAll(str, search, replacement) {
        return str.replace(new RegExp(search, 'g'), replacement);
    }
}

